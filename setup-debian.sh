#!/bin/bash
# $REPOROOT always contains the full path (except the basename), whereas
# $0 is only the relative path of the script from wherever it was called
REPOROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Notify user that the following functions take a little while
echo -n "Gathering system information. Please wait..."

# Import function definitions
source "$REPOROOT/cfg/pkg.sh"     # Pkg list configurations
source "$REPOROOT/util/init.sh"   # Global vars
initialize "$@"                   # Load platform-agnostic variables
hw_detect                         # Determine system form-factor
source "$REPOROOT/cfg/cfg.sh"     # Global presets
load_hw_cfg                       # Load platform-specific variables
source "$REPOROOT/cfg/echo.sh"    # Configure output preferences
load_echo_cfg
source "$REPOROOT/util/echo.sh"   # Beautify output
init_echo_prepends
source "$REPOROOT/util/vm.sh"     # VM creation helper functions
source "$REPOROOT/util/apt.sh"    # Apt configurations
source "$REPOROOT/util/device.sh" # Device operations
source "$REPOROOT/util/fs.sh"     # Filesystem operations
source "$REPOROOT/cfg/net.sh"     # Network configuration
source "$REPOROOT/util/net.sh"    # Networking operations
source "$REPOROOT/cfg/vm.sh"      # virt-install VM instantiations
source "$REPOROOT/cfg/device.sh"  # Platform-specific configurations

# Performed prior to user input (NO SYSTEM MODIFICATIONS BEFORE CONFIRMATION)
load_preset                # load configuration profiles
read_net_cfg_from_csv      # read IP addresses from CSV file
survey_network             # determine network access
read_net_config            # read input args and assign to networking parameters
dev_check_paths            # get device paths from labels
dev_check_exists           # check that each of the devices exists
dev_check_mounted          # check that each of the devices are mounted
dev_check_want_mounted     # check if each device is intended to be mounted
dev_check_properly_mounted # check if each device is properly mounted
dev_check_if_nbd           # test if any virtual drives are referenced
echo_delete_line           # clear the "please wait" line

if $NBD_REQUESTED; then
    # TODO: What if qemu-utils already installed?
    echo    "Virtual filesystem request detected."
    echo    "Filesystem modifications required to display accurate info:"
    echo    "  - enable nbd kernel module"
    echo    "  - install qemu-utils"
    read -p "Press enter to proceed with configuration or press Ctrl+C to exit."
    echo    "Continuing."

    nbd_enable

    # Require qemu-nbd (included in qemu-utils package)
    if ! pkg_installed qemu-utils && ! install_apt_pkg qemu-utils; then
        echo_failure -e "$_INSTALL Unable to install qemu-utils, therefore unable to perform requested operations. Exiting..."
        exit
    fi

    dev_connect_if_nbd
    echo ""
fi

print_menu # print user menu

read -p "Press enter to proceed with configuration or press Ctrl+C to exit."
echo    "Continuing."

# Performed after consent of user
if $SET_METERING; then
    set_network_metering # set metered flag based on active SSID
fi

# TODO: Unsure of where to place this
#network_is_metered   # check if the active SSID is metered

dev_mount                  # mount or re-mount devices at intended paths
dev_check_properly_mounted # update proper mount flags since devices are mounted
if $SW_UPDATE; then
    if $SW_INSTALL_KEYS; then
        task_apt_sources # get signing keys and set up mirrors
    fi
    task_update      # run apt-get autoclean/update
else
    echo_skip "$_UPDATE Software not updated... feature not selected"
fi
task_bluetooth
task_wlan

if $ADD_HOSTS; then
    add_hosts # add entries to /etc/hosts
fi

if $PERSISTENT_HOME; then
    if $HOME_PROPERMOUNT; then
        echo_blank -n "$_CONFIG Establishing a persistent home environmment"
        task_persistent_home
        echo_success -e "$_CONFIG Established a persistent home environment"
    else
        echo_failure "$_CONFIG Persistent home not established... partition not mounted"
    fi
else
    echo_skip "$_CONFIG Persistent home not established... feature not selected"
fi

read_pkg_lists # establish install/uninstall lists
if $SW_INSTALL; then
    if $SW_INSTALL_MIRROR; then
        task_install apt-mirror
    else
        echo_skip "$_INSTALL apt-mirror not installed... feature not selected"
    fi

    case $PRESET in
        "server")
            task_install "${SERVER_INSTALL_LIST[@]}"
            ;;
        *)
            task_install "${CLI_INSTALL_LIST[@]}"
            task_install "${DESKTOP_INSTALL_LIST[@]}"
            ;;
    esac

    if $SW_INSTALL_DESIGN; then
        task_install "${DESIGN_INSTALL_LIST[@]}"
    else
        echo_skip "$_INSTALL Software (design) not installed... feature not selected"
    fi

    if $SW_INSTALL_DEV; then
        task_install "${DEV_INSTALL_LIST[@]}"
    else
        echo_skip "$_INSTALL Software (dev) not installed... feature not selected"
    fi

    if $SW_INSTALL_DOCKER; then
        echo_blank -n "$_INSTALL Installing \"docker.io\"..."
        task_install docker.io
        echo_success -e "$_INSTALL Installed \"docker.io\""
    else
        echo_skip "$_INSTALL Software (docker) not installed... feature not selected"
    fi

    if $SW_INSTALL_GAMES; then
        if $GAMES_PROPERMOUNT; then
            task_install "${GAME_INSTALL_LIST[@]}"
        else
            echo_failure "$_INSTALL Software (games) not installed... partition not mounted"
        fi
    else
        echo_skip "$_INSTALL Software (games) not installed... feature not selected"
    fi

    if $SW_INSTALL_MEDIA; then
        task_install "${MEDIA_INSTALL_LIST[@]}"
    else
        echo_skip "$_INSTALL Software (media) not installed... feature not selected"
    fi

    if $SW_INSTALL_SOCIAL; then
        task_install "${SOCIAL_INSTALL_LIST[@]}"
    else
        echo_skip "$_INSTALL Software (social) not installed... feature not selected"
    fi

    if $SW_INSTALL_STEAM; then
        if $GAMES_PROPERMOUNT; then
            if $INTERNET_CONNECTED; then
                task_install steam
            else
                echo_failure "$_INSTALL Software (steam) not installed... no internet connection"
            fi
        else
            echo_failure "$_INSTALL Software (steam) not installed... partition not mounted"
        fi
    else
        echo_skip "$_INSTALL Software (steam) not installed... feature not selected"
    fi

    if $SW_INSTALL_VM; then
        if $VM_HEADLESS; then
            task_install "${VM_HEADLESS_INSTALL_LIST[@]}"
        else
            task_install "${VM_INSTALL_LIST[@]}"
        fi
        if [ "$PRESET" == "server" ]; then
            vm_init     # Start libvirtd service and network
            link_all_vm # Traverse VM list; link VMs with existing storage files
        fi
    else
        echo_skip "$_INSTALL Software (VM) not installed... VM installation not selected"
    fi
else
    echo_skip "$_INSTALL No software installed... feature not selected"
fi

if $SW_PURGE; then
    task_uninstall "${UNINSTALL_LIST[@]}" # don't comment pre/post because sw pkgs output on their own lines
else
    echo_skip "$_UNINSTALL Software not uninstalled... feature not selected"
fi

if $SW_UPGRADE; then
    echo_blank -n "$_UPGRADE Upgrading software..."
    #task_upgrade      # normal upgrade procedure
    task_dist_upgrade # upgrade packages normally kept back
    echo_success -e "$_UPGRADE Upgraded software"
else
    echo_skip "$_UPGRADE Software not upgraded... feature not selected"
fi

# TODO: Find a more elegant place for this
if [ "$PRESET" == "server" ]; then
    # TODO: Reduce screen brightness (no $DISPLAY since it's a terminal)
    echo -e '#!/bin/bash\nsetterm --blank=force; read ans; setterm --blank=poke' | sudo tee /usr/bin/blankscreen > /dev/null
    sudo chmod 755 /usr/bin/blankscreen

    # In case a laptop is being used as a server
    lid_close_suspend_disable
fi

if $PERSONALIZE; then
    echo_blank -n "$_CONFIG Applying personalizations..."
    source "$REPOROOT/cfg/gnome.sh" # Configure GNOME desktop preferences
    task_personalize
    echo_success -e "$_CONFIG Applied personalizations"
else
    echo_skip "$_CONFIG No personalizations applied... feature not selected"
fi

if $CREATE_SWAPFILE; then
    echo_blank -n "$_CONFIG Creating swapfile..."
    # TODO: Check for success before asserting that it was properly created
    create_swapfile "$SWAPFILEPATH" $SWAPFILESIZE
    sudo swapon "$SWAPFILEPATH"
    echo_success -e "$_CONFIG Created swapfile"
else
    echo_skip "$_CONFIG Skipping swapfile... feature not selected"
fi