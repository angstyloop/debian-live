#!/bin/bash

# Depends: cfg/cfg.sh
# Depends: util/init.sh - initialize

# Usage: add_arch <architecture>
function add_arch {
    echo_blank -n "$_CONFIG Adding architecture \"$1\"..."
    sudo dpkg --add-architecture $1
    echo_success -e "$_CONFIG Added architecture \"$1\""
}

# Usage: append_unique_string_to_file <string-to-insert> <filepath> <opt:sudo>
function append_unique_string_to_file {
    if [ $# -eq 3 ] && [ "$3" == "sudo" ]; then
        local prep="sudo"
    else
        local prep=""
    fi
    if ! grep -q "$1" $2; then
        echo $1 | $prep tee -a $2 > /dev/null
        return $SUCCESS
    else
        return $FAILURE
    fi
}

################################################################################
# Display
# TODO: Test this function on a variety of computers
function calibrate_display {
    local displayname="$(xrandr -q | grep " connected" | cut -d' ' -f1)"
    local modeline=$(cvt $DISPLAY_RES | grep Modeline | cut -d' ' -f2-)
    #echo $displayname
    #echo $modeline
    xrandr --newmode $modeline
    xrandr --addmode $displayname $(echo $modeline | cut -d' ' -f1)
}

# Usage:   create_swapfile <swapfile-path> <capacity-in-GB>
# Purpose: Enable swapfile to mitigate low memory concerns
# NOTE:    When booting a live image, this file must reside on a mounted drive
#          if > ~3GB
function create_swapfile {
    if [ $# -ne 2 ]; then
        echo "ERROR: Not creating swapfile; insufficient input arguments"
        return $FAILURE
    fi
    if [ -f $1 ]; then
        sudo rm $1
    fi
    sudo mkdir -p $(dirname $1)
    sudo fallocate -l $2G $1
    sudo chmod 600 $1
    sudo mkswap $1 1> /dev/null
}

function is_a_broken_symlink {
    if [ -L $1 ] && [ ! -d $1/ ]; then return $SUCCESS; else return $FAILURE; fi
}

# Checks if a given path exists and sets it to a given variable
# Usage: path_exists <path> <variable name>
function path_exists {
    if [ $# -ne 2 ]; then
        if [ -e "$1" ]; then return $SUCCESS; else return $FAILURE; fi
    else
        if [ -e "$1" ]; then eval $2=true; else eval $2=false; fi
    fi
}

# Remove a symlink if it exists and update its path
# usage update_symlink <data source> <symlink dest path> <opt:sudo>
# WARNING: BE CAREFUL!!! This function CAN DESTROY DATA!!!
function update_symlink {
    if [ $# -eq 3 ] && [ "$3" == "sudo" ]; then
        local prep="sudo "
    else
        local prep=""
    fi
    if [ ! -L "$2" ]; then
        $prep rm -rf "$2"
    fi
    $prep ln -fns "$1" "$2"
}

# Checks to see if a variable is null; output error if it is
# TODO: Test this; consider simply using -n/-z
function var_is_defined {
    if [ -z "${!1}" ]; then
        echo_var_ndef $1
        return $FAILURE
    fi
    return $SUCCESS
}
