#!/bin/bash

# Detect if running a graphical environment or a server (headless) one
function hw_detect {
    if [ -z "$DISPLAY" ]; then
        PLATFORM=server
    else
        # TODO: Add elif to know if desktop or laptop (using laptop-detect?)
        PLATFORM=laptop
    fi
}

function initialize {
    ################################################################################
    # Global configuration, regardless of environment

    ARCH=$(dpkg --print-architecture)

    # Input arguments to this script
    ARGC=$#
    ARGV=("$@")

    # Return codes
    SUCCESS=0
    FAILURE=1
    SKIP=2
}
