#!/bin/bash

# Depends on cfg/cfg.sh
# Depends on util/echo.sh

function vm_init {
    # libvirtd needs to be running in order to connect to the socket
    sudo systemctl enable --now libvirtd
    sudo virsh net-autostart --network default &> /dev/null
    sudo virsh net-start default &> /dev/null

    # Only doing this to avoid permissions issues when reading VM images
    # TODO: This is not good practice
    sudo chmod 777 /media/$USER
    sudo chmod 777 $VM_MNT_PATH
}

# Usage: vm_autostart <vm name>
function vm_autostart {
    echo_blank -n "$_CONFIG Starting $1"
    sudo virsh autostart $1 1> /dev/null
    sudo virsh start $1 1> /dev/null
    echo_success -e "$_CONFIG Started $1"
}

# Usage: Not intended to be called directly, but through vm_link
function vm_env_cfg {

    LIBVIRT_IMAGES=/var/lib/libvirt/images
    LIBVIRT_CONFIG=/etc/libvirt/qemu
    LIBVIRT_SNAPSHOT=/var/lib/libvirt/qemu/snapshot

    # Don't persist variables through VM instances
    VM_ARCH=""           # Ex: aarch64 | x86_64 | ...
    VM_BOOT=""           # Ex: hd | uefi | ...
    VM_DESCRIPTION=""    # Description of the VM
    VM_DISK_INTERFACE="" # Ex: ide | sata | virtio | ...
    VM_FILETYPE=""       # VM disk image filetype
    VM_NAME=""           # VM disk image filename (no filetype)
    VM_NET_IP=""         # Static virtual IP address for server images
    VM_NET_MAC=""        # Static MAC address for server images
    VM_OSTYPE=""         # Ex: linux | windows
    VM_OSVARIANT=""      # Ex: centos7.0 | debian10 | winxp | win10 | ...
    VM_RAM=""            # Ex: 1024 | 4096 | ...
    VM_SERVER=""         # Cfg networking as server/headless if yes, else client
    VM_TITLE=""          # Title of the VM
    VM_VCPUS=""          # Ex: 1 | 4 | $(nproc)

    # Parse input arguments
    for i in "$@"; do
        case $i in
          --arch=*)
              VM_ARCH="${i#*=}"
              shift # past argument=value
              ;;
          --boot=*)
              VM_BOOT="${i#*=}"
              shift # past argument=value
              ;;
          --description=*)
              VM_DESCRIPTION="${i#*=}"
              shift # past argument=value
              ;;
          --diskinterface=*)
              VM_DISK_INTERFACE="${i#*=}"
              shift # past argument=value
              ;;
          --diskname=*)
              VM_NAME="${i#*=}"
              shift # past argument=value
              ;;
          --disktype=*)
              VM_FILETYPE="${i#*=}"
              shift # past argument=value
              ;;
          --headless=*)
              # Respect global configuration
              if [ $VM_HEADLESS -a "${i#*=}" == "yes" ]; then VM_SERVER=true; else VM_SERVER=false; fi
              shift # past argument=value
              ;;
          --os-type=*)
              VM_OSTYPE="${i#*=}"
              shift # past argument=value
              ;;
          --os-variant=*)
              VM_TITLE="${i#*=}"
              shift # past argument=value
              ;;
          --ram=*)
              VM_RAM="${i#*=}"
              shift # past argument=value
              ;;
          --static-ip=*)
              VM_NET_IP="${i#*=}"
              shift # past argument=value
              ;;
          --static-mac=*)
              VM_NET_MAC="${i#*=}"
              shift # past argument=value
              ;;
          --title=*)
              VM_TITLE="${i#*=}"
              shift # past argument=value
              ;;
          --vcpus=*)
              VM_VCPUS="${i#*=}"
              shift # past argument=value
              ;;
          --*)
              echo "vm_env_cfg: Undefined option $i"
              exit 1
              ;;
          *)
              # Ignore value
              ;;
        esac
    done

    # Use defaults if left unconfigured
    if [ -z "$VM_ARCH" ]; then VM_ARCH="x86_64"; fi
    if [ -z "$VM_BOOT" ]; then VM_BOOT="hd"; fi
    if [ -z "$VM_DESCRIPTION" ]; then VM_DESCRIPTION="Automatically configured"; fi
    if [ -z "$VM_DISK_INTERFACE" ]; then VM_DISK_INTERFACE="virtio"; fi
    if [ -z "$VM_FILETYPE" ]; then VM_FILETYPE="qcow2"; fi
    if [ -z "$VM_RAM" ]; then VM_RAM="4096"; fi
    if [ -z "$VM_NAME" ]; then VM_NAME="Default"; fi
    if [ -z "$VM_OSTYPE" ]; then VM_OSTYPE="generic"; fi
    if [ -z "$VM_OSVARIANT" ]; then VM_OSVARIANT="generic"; fi
    if [ -z "$VM_TITLE" ]; then VM_TITLE="Default"; fi
    if [ -z "$VM_VCPUS" ]; then VM_CPUS="$(nproc)"; fi

    # Establish absolute path to the VM disk image
    VM_DISK_PATH="$VM_MNT_PATH/$VM_NAME.$VM_FILETYPE"
}

# Usage: vm_net_register <vm name> <vm mac> <vm ip addr>
function vm_net_register {
    # Print: sudo virsh net-dhcp-leases default
    sudo virsh net-update default add ip-dhcp-host "<host mac='$2' name='$1' ip='$3' />" --live --config &> /dev/null
}

function vm_link {
    vm_env_cfg "$@"

    if [ ! -e $VM_DISK_PATH ]; then
        echo_failure -e "$_CONFIG Not linking \"$VM_NAME\"; path \"$VM_DISK_PATH\" not found..."
    elif ! $(sudo virsh list --all | grep -q " $VM_NAME "); then
        echo_blank -n "$_CONFIG Linking VM \"$VM_NAME\"..."

        # Configure network based on whether the VM is a:
        # - client (default network), or
        # - server (direct macvtap connection)
        if $VM_SERVER; then
            local ethInterface=$(ip a | grep "eno" | head -n1 | cut -d':' -f2 | cut -d' ' -f2)
            VM_NETWORK="type=direct,source=$ethInterface,source.mode=bridge,model=virtio,mac=$VM_NET_MAC"
            vm_net_register $VM_NAME $VM_NET_MAC $VM_NET_IP
        else
            VM_NETWORK="network=default"
        fi

        # Link the VM but do not start it
        sudo virt-install \
            --arch $VM_ARCH \
            --connect qemu:///system \
            --boot $VM_BOOT \
            --import \
            --virt-type=kvm \
            --name $VM_NAME \
            --metadata title=$VM_TITLE \
            --metadata description=$VM_DESCRIPTION \
            --os-type $VM_OSTYPE \
            --os-variant detect=on,name=$VM_OSVARIANT \
            --ram $VM_RAM \
            --vcpus $VM_VCPUS \
            --graphics spice \
            --disk $VM_DISK_PATH,bus=$VM_DISK_INTERFACE,device=disk,format=$VM_FILETYPE,sparse=no \
            --network $VM_NETWORK \
            --events on_crash=restart \
            --noautoconsole \
            --noreboot \
            --sound default \
            --hvm \
            1> /dev/null
        if [ ! -d $LIBVIRT_SNAPSHOT_DIR ]; then
            sudo mkdir -p $LIBVIRT_SNAPSHOT_DIR
        fi
        # TODO: Don't specify r220; the snapshot may not be used on that system
        if [ -d "$REPOROOT/r220/snapshot/$VM_NAME" ]; then
            sudo cp -rp "$REPOROOT/r220/snapshot/$VM_NAME" $LIBVIRT_SNAPSHOT_DIR
        fi
        if $(sudo virsh list --all | grep -q " $VM_NAME "); then
            echo_success -e "$_CONFIG Linked VM \"$VM_NAME\""
        else
            echo_failure -e "$_CONFIG Unable to link VM \"$VM_NAME\""
        fi
    else
        echo_skip -e "$_CONFIG \"$VM_NAME\" already linked... skipping"
    fi

    if $VM_AUTOSTART; then vm_autostart $VM_NAME; fi
}