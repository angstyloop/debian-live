#!/bin/bash

# Depends on cfg/echo.sh

# Get configuration
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

case $TERM in
    "linux")
        # Cannot print '𐄂' or '✓'
        CHAR_NO="N"
        CHAR_YES="Y"
        ;;
    *)
        CHAR_NO="𐄂"  # '\U10102'
        CHAR_YES="✓" # '\U2713'
        ;;
esac

function init_echo_prepends {
    _CONFIG="   CONFIG:"
    _INSTALL="  INSTALL:"
    _NETWORK="  NETWORK:"
    _UNINSTALL="UNINSTALL:"
    _UPDATE="   UPDATE:"
    _UPGRADE="  UPGRADE:"
}

################################################################################
# Echo functions

function echo_usage {
    echo "USAGE: $0 wlan_ssid wlan_passphrase"
}

function echo_var_ndef {
    echo "ERROR: Variable \"$1\" referenced before assignment"
}

function echo_blank {
    if ! $PRINT_BLANK; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            echo "[ ] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    echo "[ ] $2"
                    ;;
                "-n")
                    echo -n "[ ] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            echo -e -n "[ ] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

# Clear the current output line
function echo_delete_line {
    echo -e -n "\033[2K\r"
}

function echo_failure {
    if ! $PRINT_FAILURE; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            >&2 echo "[$CHAR_NO] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    >&2 echo -e "[$CHAR_NO] $2"
                    ;;
                "-n")
                    >&2 echo -n "[$CHAR_NO] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            >&2 echo -e -n "[$CHAR_NO] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

function echo_skip {
    if ! $PRINT_SKIP; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            echo "[-] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    echo -e "[-] $2"
                    ;;
                "-n")
                    echo -n "[-] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            echo -e -n "[-] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

function echo_success {
    if ! $PRINT_SUCCESS; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            echo "[$CHAR_YES] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    echo -e "[$CHAR_YES] $2"
                    ;;
                "-n")
                    echo -n "[$CHAR_YES] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            echo -e -n "[$CHAR_YES] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

function echo_graphic {
    if [ -z "$1" ]; then
        echo -n "[!]"
    elif $1; then
        echo -n "[$CHAR_YES]"
    else
        echo -n "[$CHAR_NO]"
    fi
}

function print_menu {
    echo "STORAGE [EXISTS] [WANT MOUNTED] [MOUNTED] [MOUNTED AT DESIRED LOCATION]"

    echo -n  "-> $(echo_graphic $GAMES_EXISTS)$(echo_graphic $GAMES_WANTMOUNT)$(echo_graphic $GAMES_MOUNTED)$(echo_graphic $GAMES_PROPERMOUNT) "
    echo -en "LABEL=\"$GAMES_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$GAMES_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$GAMES_MNT_PATH\""

    echo -n  "-> $(echo_graphic $HOME_EXISTS)$(echo_graphic $HOME_WANTMOUNT)$(echo_graphic $HOME_MOUNTED)$(echo_graphic $HOME_PROPERMOUNT) "
    echo -en "LABEL=\"$HOME_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$HOME_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$HOME_MNT_PATH\""

    echo -n  "-> $(echo_graphic $REPO_EXISTS)$(echo_graphic $REPO_WANTMOUNT)$(echo_graphic $REPO_MOUNTED)$(echo_graphic $REPO_PROPERMOUNT) "
    echo -en "LABEL=\"$REPO_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$REPO_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$REPO_MNT_PATH\""

    echo -n  "-> $(echo_graphic $STEAM_LUN_EXISTS)$(echo_graphic $STEAM_LUN_WANTMOUNT)$(echo_graphic $STEAM_LUN_MOUNTED)$(echo_graphic $STEAM_LUN_PROPERMOUNT) "
    echo -en "LABEL=\"$STEAM_LUN_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$STEAM_LUN_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$STEAM_LUN_MNT_PATH\""

    echo -n  "-> $(echo_graphic $VM_EXISTS)$(echo_graphic $VM_WANTMOUNT)$(echo_graphic $VM_MOUNTED)$(echo_graphic $VM_PROPERMOUNT) "
    echo -en "LABEL=\"$VM_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$VM_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$VM_MNT_PATH\""

    echo -e "\nNETWORK"
    echo -e "-> Connection: internet\t\t$(echo_graphic $INTERNET_CONNECTED)"
    if $INTERNET_CONNECTED; then
        echo -e "|  -> Active SSID\t\t\"$ACTIVE_SSID\""
    fi
    echo -e "-> NAS\t\t\t\t$(echo_graphic $NAS_EXISTS)$(echo_graphic $NAS_WANTMOUNT)$(echo_graphic $NAS_MOUNTED)$(echo_graphic $NAS_PROPERMOUNT)"
    echo -e "-> Offline\t\t\t$(echo_graphic $OFFLINE)"
    echo -e "-> Metered network\t\t$(echo_graphic $METERED_NETWORK)"
    echo -e "-> Enable Bluetooth\t\t$(echo_graphic $USE_BLUETOOTH)"
    echo -e "-> Enable WiFi ($WIFI_IF)\t\t$(echo_graphic $USE_WIFI)"
    if $USE_WIFI; then
        echo -e "-> Configure networking\t\t$(echo_graphic $config_wlan)"
        if $config_wlan; then
            echo -e "  -> WLAN SSID\t\t\t\"$wlan_ssid\""
            echo -e "  -> WLAN passphrase\t\t\"$wlan_pass\""
        fi
    fi
    echo ""

    echo SOFTWARE
    echo -e "-> Preset\t\t\t\"$PRESET\""
    echo -e "-> Use repositories\t\t\"$COMPONENTS\" in \"$SUITE\""
    echo -e "-> Update\t\t\t$(echo_graphic $SW_UPDATE)"
    echo -e "-> Install\t\t\t$(echo_graphic $SW_INSTALL)"
    echo -e "|  -> apt-mirror\t\t$(echo_graphic $SW_INSTALL_MIRROR)"
    echo -e "|  -> Design\t\t\t$(echo_graphic $SW_INSTALL_DESIGN)"
    echo -e "|  -> Dev\t\t\t$(echo_graphic $SW_INSTALL_DEV)"
    echo -e "|  -> docker\t\t\t$(echo_graphic $SW_INSTALL_DOCKER)"
    echo -e "|  -> Games\t\t\t$(echo_graphic $SW_INSTALL_GAMES)"
    echo -e "|  |   -> steam\t\t\t$(echo_graphic $SW_INSTALL_STEAM)"
    echo -e "|  -> Media\t\t\t$(echo_graphic $SW_INSTALL_MEDIA)"
    echo -e "|  -> Social\t\t\t$(echo_graphic $SW_INSTALL_SOCIAL)"
    echo -e "|  -> VM environment\t\t$(echo_graphic $SW_INSTALL_VM)"
    echo -e "|      -> Headless\t\t$(echo_graphic $VM_HEADLESS)"
    echo -e "->Remove\t\t\t$(echo_graphic $SW_PURGE)"
    echo -e "->Upgrade\t\t\t$(echo_graphic $SW_UPGRADE)"
    echo    ""

    echo PERSONALIZATION
    echo -e "-> Persistent home\t\t$(echo_graphic $PERSISTENT_HOME)"
    echo -e "-> Personalize\t\t\t$(echo_graphic $PERSONALIZE)"
    if $PERSONALIZE; then
        echo -e "  -> Name\t\t\t\"$NAME\""
        echo -e "  -> Email\t\t\t\"$EMAIL\""
        echo -e "  -> Brightness (0.0-1.0)\t$BRIGHTNESS"
        echo -e "  -> Night night\t\t$NIGHT_LIGHT"
        echo -e "  -> Time zone\t\t\t\"$TIME_ZONE\""
    fi
    echo ""
}