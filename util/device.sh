#!/bin/bash

# Depends: cfg/cfg.sh - NAS_SHARE_LIST
# Depends: cfg/net.sh - IP_NAS_ETH
# Depends: util/net.sh - IP_NAS_ETH

################################################################################
# Device functions
# TODO: Make these work for mounted NAS shares - not only block devices
# TODO: Make these work with virtual filesystems (.img,.qcow2,etc)

# Input:  partition_name_exists <name>
# Output: name or "" if one doesn't exist
function partition_name_exists {
    if lsblk -l -n -o LABEL -p | grep -q $1; then
        echo $1
    else
        >&2 echo "$_CONFIG Partition name \"$1\" does not exist in lsblk"
        exit $FAILURE
    fi
}

# Usage: device_path_exists <device_path>
# Output: device_path or "" if one doesn't exist
function device_path_exists {
    # TODO: Should we simply [ -e $1 ]; then ...?
    if lsblk -l -n -o NAME -p | grep -q $1; then
        echo $1
    else
        >&2 echo "$_CONFIG Device path \"$1\" does not exist in lsblk"
        exit $FAILURE
    fi
}

# Usage: mount_point_exists <mount_point>
# Output: mountpoint or "" if one doesn't exist
function mount_point_exists {
    if lsblk -l -n -o MOUNTPOINT -p | grep -q $1; then
        echo $1
    else
        >&2 echo "$_CONFIG Mount point \"$1\" does not exist in lsblk"
        exit $FAILURE
    fi
}

# Input:  get_device_path_from_label <label>
# Output: device_path or "" if one does not exist
function get_device_path_from_label {
    if [ -z "$1" ]; then
        # TODO: Investigate why this would be null
        >&2 echo "$_CONFIG Skipping empty device label"
        exit $FAILURE
    fi
    local path=$(lsblk -l -n -o NAME,LABEL -p | grep $1 | cut -d' ' -f 1)
    if [ -z "$path" ]; then
        >&2 echo "$_CONFIG No device path found for device label \"$1\" in lsblk"
        exit $FAILURE
    fi
    if [ $(echo $path | wc -w) -gt 1 ]; then
        # TODO: Only include devices with lables exactly matching query - not just containing it
        >&2 echo "$_CONFIG More than one device containing device label \"$1\" in lsblk"
        >&2 echo "$_CONFIG (Received \"$path\")"
        exit $FAILURE
    fi
    echo $path
}

# Input:  get_device_label_from_path <device_path>
# Output: device_label or "" if one does not exist
function get_device_label_from_path {
    local path=$(lsblk -n -o LABEL -p $1)
    if [ -z "$path" ]; then
        >&2 echo "$_CONFIG No device label found for device path \"$1\" in lsblk"
        exit $FAILURE
    fi
    echo $path
}

# TODO: Not used
# Input:  get_mount_path_from_label <label>
# Output: mount_path or "" if one does not exist
function get_mount_path_from_label {
    if [ -z "$(partition_name_exists $1)" ]; then
        >2& echo ""
        >&2 echo "$_CONFIG No mount path for device label \"$1\" in blkid"
        exit $FAILURE
    else
        local pre_substring=$(df | grep $1 | cut -d'%' -f2)
        echo ${pre_substring:1}
    fi
}

# Input:  get_mount_path_from_device_path <device_path>
# Output: mount_path or "" if one does not exist
function get_mount_path_from_device_path {
    # TODO: What happens if it is mounted in two places? (e.g. /media/user/repo AND /var/spool/apt-mirror)?
    echo $(lsblk $1 -n -o MOUNTPOINT | grep /)
    #path=$(lsblk $1 -n -o MOUNTPOINT | grep /)
    #if [ -z "$path" ]; then
        # Device is not mounted
        #>&2 echo "$_CONFIG No mount path found for device path \"$1\" in lsblk"
    #    exit $FAILURE
    #fi
    #echo $path
}

# Usage: dev_mounted <device_path>
function dev_mounted {
    # The space following $1 is intentional to avoid false positives
    # TODO: Doesn't seem to work for /dev/nbd*
    if grep -qs "$1 " /proc/mounts; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Usage: verify_mount_path <device_path> <desired_mount_path>
function verify_mount_path {
    if [ "$(get_mount_path_from_device_path $1)" == "$2" ]; then
        return $SUCCESS
    fi
    return $FAILURE
    # If requested device_path and requested mount_path are in the same entry
    #if lsblk -l -n -o NAME,MOUNTPOINT -p | grep $1 | grep -q $current_mount_path; then
    #    return $SUCCESS
    #fi
    #return $FAILURE
}

# Usage: mount_if_not_mounted <device_path> <mount_path>
# Dependent on running dev_check_paths first
function mount_if_not_mounted {
    # TODO: Does not currently work with nfs mounts (parsing the server:path format)
    # TODO: Streamline redundant logic
    if [ ! -e $1 ]; then echo "ERROR: Expected device path \"$1\" not found!"; return $FAILURE; fi
    if dev_mounted $1; then
        echo_blank -n "$_CONFIG \"$1\" already mounted, but ensuring it's mounted at \"$2\"..."
        local current_mount_path=$(get_mount_path_from_device_path $1)
        if verify_mount_path $1 "$2"; then
            echo_delete_line # Formatting otherwise wrong if not printing skips
            echo_skip -e "$_CONFIG Not mounting \"$1\" to \"$2\"... already there"
            return $SUCCESS
        else
            # Re-mount
            sudo umount $current_mount_path
            if [ ! -d "$2" ]; then
                sudo mkdir -p "$2"
                sudo chown $USER:$USER "$2"
            fi
            sudo mount $1 "$2"

            if verify_mount_path $1 "$2"; then
                echo_success -e "$_CONFIG Re-mounted \"$1\" from \"$current_mount_path\" to \"$2\""
                return $SUCCESS
            else
                echo_failure -e "$_CONFIG Failed to mount \"$1\" to desired path \"$2\""
                return $FAILURE
            fi
        fi
    else
        echo_blank -n "$_CONFIG Mounting \"$1\" to \"$2\"..."
        # Prepare the mount path
        if [ ! -d "$2" ]; then
            sudo mkdir -p "$2"
            sudo chown $USER:$USER "$2"
        fi
        sudo mount $1 "$2"

        if verify_mount_path $1 "$2"; then
            echo_success -e "$_CONFIG Mounted device \"$1\" at \"$2\""
            return $SUCCESS
        else
            echo_failure -e "$_CONFIG Failed to mount \"$1\" to desired path \"$2\""
            return $FAILURE
        fi
    fi
}

# Usage:   mount_scsi <portal> <port> <target> <partition label> <mount path>
# Depends: open-iscsi installed
# Depends: ntfs-3g    installed
#          iSCSI drive MUST be MBR/NTFS
function mount_scsi {
    if ! pkg_installed open-iscsi; then
        echo_failure -e "$_CONFIG Cannot configure iscsi... open-iscsi not installed"
        return $FAILURE
    elif ! pkg_installed ntfs-3g; then
        echo_failure -e "$_CONFIG Cannot configure iscsi... ntfs-3g not installed"
        return $FAILURE
    fi
    sudo systemctl start iscsid
    sudo mkdir -p $5
    sudo iscsiadm --mode discovery -t sendtargets --portal $1:$2 1> /dev/null
    sudo iscsiadm --mode node --targetname $3 --portal $1:$2 --login 1> /dev/null
    sudo mount -t ntfs $(get_device_path_from_label $4 2> /dev/null) $5
}

# Usage:   mount_share <nas share(s)>
# Depends: util/net.sh due to IP_NAS_ETH
# TODO:    Remove dependency on IP address (figure out how to have the IP
#          address as the 1st input argument and the NAS share list be all the
#          subsequent arguments.
function mount_share {
    if [ $# -eq 0 ]; then
        return $FAILURE
    fi
    local pkglist=("$@")
    local rtn_code=$SUCCESS
    for i in "${pkglist[@]}"; do
        local lower=${i,,}
        local destpath="$NAS_ROOT/$lower"
        sudo mkdir -p $destpath
        if ! df | grep -q $lower; then
            sudo mount -t nfs $IP_NAS_ETH:/data/$i $destpath
            if dev_mounted $IP_NAS_ETH:/data/$i; then
                echo_success -e "$_CONFIG Mounted share \"$i\" at \"$destpath\""
            else
                echo_failure -e "$_CONFIG Failed to mount \"$1\" to desired path \"$destpath\""
                rtn_code=$FAILURE
            fi
        else
            echo_skip -e "$_CONFIG Not mounting \"$i\" to \"$destpath\"... already mounted"
        fi
    done
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

# Mount every share available on the NAS, based on NAS_SHARE_LIST
# Depends: cfg/cfg.sh due to NAS_SHARE_LIST
function mount_nas {
    if ! $NAS_EXISTS; then
        return $FAILURE
    fi
    local rtn_code=$SUCCESS
    if ! mount_share "${NAS_SHARE_LIST[@]}"; then rtn_code=$FAILURE; fi

    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

# Assign a virtual filesystem to a Network Block Device (nbd) device path
# Usage: nbd_connect <device path> <virtual filesystem filepath>
function nbd_connect {
    local rtn_code=$SUCCESS
    echo_blank -n "$_CONFIG Connecting \"$2\" to $1..."
    if [ -n "$(device_path_exists $1 2> /dev/null)" ]; then
        echo_delete_line # Formatting otherwise wrong if not printing skips
        echo_skip -e "$_CONFIG Not connecting \"$2\" to $1... already connected"
    elif ! pkg_installed qemu-utils; then
        echo_failure -e "$_CONFIG Cannot connect \"$2\" to $1... qemu-utils not installed"
        rtn_code=$FAILURE
    else
        sudo qemu-nbd --connect=$1 "$2" 2> /dev/null
        if [ -n "$(device_path_exists $1 2> /dev/null)" ]; then
            echo_success -e "$_CONFIG Connected \"$2\" to $1"
        else
            echo_failure -e "$_CONFIG Failed to connect \"$2\" to $1"
            rtn_code=$FAILURE
        fi
    fi
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

# Unassign a virtual filesystem from a Network Block Device (nbd) device path
# Usage: nbd_disconnect <device path>
function nbd_disconnect {
    local rtn_code=$SUCCESS
    echo_blank -n "$_CONFIG Disconnecting $1..."
    if [ -z "$(device_path_exists $1 2> /dev/null)" ]; then
        echo_delete_line # Formatting otherwise wrong if not printing skips
        echo_skip -e "$_CONFIG Not disconnecting $1... not connected"
        rtn_code=$FAILURE
    elif ! pkg_installed qemu-utils; then
        # Not sure how this is possible, but... just in case?
        echo_failure -e "$_CONFIG Cannot disconnect $1... qemu-utils not installed"
        rtn_code=$FAILURE
    else
        sudo qemu-nbd --disconnect $1
        if [ -z "$(device_path_exists $1 2> /dev/null)" ]; then
            echo_success -e "$_CONFIG Disconnected $1"
        else
            echo_failure -e "$_CONFIG Failed to disconnect $1"
            rtn_code=$FAILURE
        fi
    fi
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

# Enable support for Network Block Devices (nbd)
function nbd_enable {
    if lsmod | grep -q nbd; then
        echo_skip -e "$_CONFIG Not enabling module nbd... already enabled"
        return $FAILURE
    fi
    local rtn_code=$SUCCESS
    echo_blank -n "$_CONFIG Enabling module nbd..."
    if [ $(ls /dev/nbd* 2> /dev/null | wc -l) -gt 0 ]; then
        echo_delete_line # Formatting otherwise wrong if not printing skips
        echo_skip -e "$_CONFIG Not enabling module nbd... already enabled"
        rtn_code=$FAILURE
    else
        sudo modprobe nbd max_part=8
        if [ $(ls /dev/nbd* 2> /dev/null | wc -l) -gt 0 ]; then
            echo_success -e "$_CONFIG Enabled module nbd"
        else
            echo_failure -e "$_CONFIG Failed to enable module nbd"
            rtn_code=$FAILURE
        fi
    fi
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

# Disable support for Network Block Devices (nbd)
function nbd_disable {
    echo_blank -n "$_CONFIG Disconnecting nbd devices..."
    nbd_disconnect /dev/nbd*
    echo_success -e "$_CONFIG Disconnected nbd devices"
    if ! lsmod | grep -q nbd; then
        echo_skip -e "$_CONFIG Not disabling module nbd... already disabled"
        return $FAILURE
    fi
    echo_blank -n "$_CONFIG Disabling nbd module..."
    sudo rmmod nbd
    echo_success -e "$_CONFIG Disabled nbd module"
}

# Disable laptop lid closure suspend event
function lid_close_suspend_disable {
    echo_blank -n "$_CONFIG Configuring systemd to ignore laptop lid closure event..."
    # Disable sleep/suspend/hibernate/hybrid services
    #sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target > /dev/null
    sudo sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/g' /etc/systemd/logind.conf
    sudo sed -i 's/#HandleLidSwitchExternalPower=suspend/HandleLidSwitchExternalPower=ignore/g' /etc/systemd/logind.conf
    sudo systemctl restart systemd-logind
    echo_success -e "$_CONFIG Configured systemd to ignore laptop lid closure event"
}
