#!/bin/bash

# Depends: cfg/cfg.sh
# Depends: util/fs.sh - var_is_defined, path_exists

# TODO: A lot of this logic can be automated, e.g. detect if connected to ethernet
#       and prefer that over wifi, etc.

################################################################################
# Networking functions

# Read IP addresses into variable names based on external csv spreadsheet
# Usage read_net_cfg_from_csv <opt: /path/to/csv>
function read_net_cfg_from_csv {
    # set a default path
    if [ "$#" -ne 1 ]; then
        local path=$REPOROOT/cfg/net.csv
    else
        local path=$1
    fi

    # read the csv file but skip the header
    while IFS="," read -r varname ipv4 mac comment
    do
        # skip empty required fields
        if [ -z $varname -o -z $ipv4 ]; then continue; fi
        # assign the ip address to a variable name
        eval $varname=$ipv4
    done < <(tail -n +2 $path)
}

# Usage get_mac_from_ip <IP address>
function get_mac_from_ip {
    ping -c 1 $1 > /dev/null && /usr/sbin/arp -a | grep $1 | cut -d' ' -f4
}

# Usage: test_connection <IP address> <send N packets> <wait N seconds>
function test_connection {
    case $# in
        0)
            return $SKIP
            ;;
        1)
            ping -c 1 $1 &> /dev/null
            ;;
        3)
            ping -c $2 -i $3 $1 &> /dev/null
            ;;
        *)
            >&2 echo "ERROR: test_connection: unsupported number of input arguments"
            return $FAILURE
            ;;
    esac
    if [ $? -eq 0 ]; then return $SUCCESS; else return $FAILURE; fi
}

# Usage: wait_for_internet_connection <send N packets> <wait N seconds>
function wait_for_internet_connection {
    local deb_debian_org=199.232.38.132
    test_connection $deb_debian_org $1 $2
    if [ $? -eq 0 ]; then return $SUCCESS; else return $FAILURE; fi
}

function read_net_config {
    # USE_WIFI:    WiFi enable
    # config_wlan: automatic WiFi configuration (only relevant when WiFi enabled)
    case $ARGC in
        0)
            config_wlan=false
            ;;
        1)
            if ! $USE_WIFI; then
                config_wlan=false
                echo_skip "$_NETWORK Not configuring WLAN as WIFI is selected to be turned off"
            else
                echo_usage
                wlan_ssid=${ARGV[0]}
                read -p "$_NETWORK Passphrase for WLAN (SSID = ${ARGV[0]}): " wlan_pass
                if [ -z "$wlan_pass" ]; then
                    config_wlan=false
                fi
                config_wlan=true
            fi
            ;;
        2)
            if ! $USE_WIFI; then
                config_wlan=false
                echo_skip "$_NETWORK Not configuring WLAN as WIFI is selected to be turned off"
            else
                wlan_ssid=${ARGV[0]}
                wlan_pass=${ARGV[1]}
                config_wlan=true
            fi
            ;;
        *)
            echo_usage
            exit $FAILURE
            ;;
    esac
    return $SUCCESS
}

# Depends: util/fs.sh - path_exists
function generate_wpa {
    echo_blank -n "$_NETWORK Generating \"$WPA_SUPPLICANT_CFG\"..."

    is_bssid=0

    if path_exists $WPA_SUPPLICANT_CFG; then
        sudo mv $WPA_SUPPLICANT_CFG $WPA_SUPPLICANT_CFG.bak
    fi
    # Generate config file
    wpa_passphrase "$wlan_ssid" "$wlan_pass" | sudo tee $WPA_SUPPLICANT_CFG 1> /dev/null

    # Remove commented line containing plaintext passphrase
    sudo sed -i /#/d $WPA_SUPPLICANT_CFG

    #sudo sed -i '1 i ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=wheel' $WPA_SUPPLICANT_CFG # 'wheel' group must exist to use this
    #sudo sed -i "$i        scan_ssid=|r /dev/stdin" $WPA_SUPPLICANT_CFG <<<$is_bssid
    sudo sed -i '$i\        proto=RSN' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        key_mgmt=WPA-PSK' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        group=CCMP' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        pairwise=CCMP' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        priority=10' $WPA_SUPPLICANT_CFG

    echo_success -e "$_NETWORK Generated \"$WPA_SUPPLICANT_CFG\""
}

function task_bluetooth {
    if ! $USE_BLUETOOTH; then
        echo_blank -n "$_NETWORK Disabling bluetooth..."
        sudo systemctl stop bluetooth
        echo_success -e "$_NETWORK Disabled bluetooth"
    else
        echo_skip "$_NETWORK Bluetooth enabled but not configured; feature not yet implemented"
    fi
}

function task_wlan {
    # Check for undefined variables
    if ! var_is_defined USE_WIFI; then return $FAILURE; fi
    if ! var_is_defined config_wlan; then return $FAILURE; fi

    if ! $USE_WIFI; then
        echo_skip "$_NETWORK WLAN not configured... WiFi not selected"
        return $SUCCESS
    fi
    if ! $config_wlan; then
        echo_skip "$_NETWORK WLAN not configured... config_wlan said to skip"
        return $SUCCESS
    fi
    generate_wpa
    echo_blank -n "$_NETWORK Restarting wpa_supplicant..."
    sudo systemctl restart wpa_supplicant
    echo_success -e "$_NETWORK Restarted wpa_supplicant"

    echo_blank -n "$_NETWORK Connecting wpa_supplicant..."
    sudo wpa_supplicant -B -i$WIFI_IF -c $WPA_SUPPLICANT_CFG -Dnl80211,wext 1> /dev/null
    echo_success -e "$_NETWORK Connected wpa_supplicant"

    echo_blank -n "$_NETWORK Enabling DHCP on interface $WIFI_IF..."
    sudo dhclient $WIFI_IF
    echo_success -e "$_NETWORK Enabled DHCP on interface $WIFI_IF"

    if ! $METERED_NETWORK; then
        # TODO: Wait for active internet connection before proceeding; continue
        #       upon the 1st successful ping
        echo_blank -n "$_NETWORK Establishing internet connection..."
        wait_for_internet_connection 12 5
        echo_success -e "$_NETWORK Established internet connection"
    fi
}

function survey_network {
    # Check for undefined variables
    if ! var_is_defined FORCE_OFFLINE; then FORCE_OFFLINE=false; fi

    WIFI_IF="$(basename $(find /sys/class/net -type l | grep -v eno | grep -v lo | grep -v usb | grep -v virbr | grep -v vnet))"
    # TODO: Ensure nmcli is installed
    ACTIVE_SSID="$(nmcli -t -f active,ssid dev wifi | grep '^yes' | cut -d':' -f2)"

    if test_connection $IP_NAS_ETH; then NAS_EXISTS=true; else NAS_EXISTS=false; fi
    if test_connection debian.org; then INTERNET_CONNECTED=true; else INTERNET_CONNECTED=false; fi

    if $FORCE_OFFLINE || ( ! $INTERNET_CONNECTED && ! $NAS_EXISTS ); then
        OFFLINE=true
    else
        OFFLINE=false
    fi

    # List metered networks; everything else is unmetered
    case "$ACTIVE_SSID" in
        "The Internet")
            METERED_NETWORK=true
            ;;
        *)
            # TODO: Could connect to internet via ethernet & *not* have an active SSID
            METERED_NETWORK=false
            ;;
    esac
}

# set metered flag based on active SSID
function set_network_metering {
    if ! $OFFLINE; then # TODO: May need to do: if [ -n "$ACTIVE_SSID" ]
        echo_blank -n "$_NETWORK Setting network metering..."
        if $METERED_NETWORK; then
            local ismetered="yes"
        else
            local ismetered="no"
        fi
        nmcli connection modify $ACTIVE_SSID connection.metered $ismetered
        echo_success -e "$_NETWORK Set network metering"
    else
        echo_skip "$_NETWORK Network metering not set... we are offline"
    fi
}

# Determine if the active internet connection is metered
# TODO: Improve this before use
#function network_is_metered {
#    local status=$(nmcli -f connection.metered connection show $ACTIVE_SSID)
#    case ${status##* } in
#        "unknown")
#            METERED_NETWORK=false
#            return $FAILURE # TODO: Revise this
#            ;;
#        "yes")
#            METERED_NETWORK=true
#            return $SUCCESS # TODO: Revise this
#            ;;
#        "no")
#            METERED_NETWORK=false
#            return $FAILURE # TODO: Revise this
#            ;;
#    esac
#}

