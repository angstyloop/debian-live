#!/bin/bash

# Depends on cfg/cfg.sh
# Depends on cfg/net.sh due to IP_NAS_ETH, test_connection, and ACTIVE_SSID
# Depends on util/vm.sh

DEBIAN_MIRROR="ftp.us.debian.org/debian"

# Usage: download_if_missing_from_mirror <http|https> <url>
function download_if_missing_from_mirror {
    if [ $# -ne 2 ]; then return $FAILURE; fi

    # Strip last period and everything following it
    #local dest_filename="${2%.*}"

    local dest_filename="$2"

    # Download potentially missing files from mirror,
    # as a workaround to apt-mirror not pulling all necessary filetypes
    if [ ! -f $REPO_MNT_PATH/mirror/$dest_filename ]; then
        if ! $REPO_PROPERMOUNT; then echo "WARN: Not downloading \"$2\"; repo not properly mounted..." && return $FAILURE; fi
        #if $OFFLINE; then echo "WARN: Not downloading \"$1\"; offline..." && return $FAILURE; fi
        if ! pkg_installed curl; then echo "WARN: Not downloading \"$2\"; curl not installed..." && return $FAILURE; fi
        echo "NOTE: Downloading \"$2\""
        sudo curl --url $1://$2 --output $REPO_MNT_PATH/mirror/$dest_filename &> /dev/null
        sudo chown root:root $REPO_MNT_PATH/mirror/$dest_filename
    fi
}

function pkg_installed {
    # MUST compare to 0... NOT arbitrary YES/NO value
    # If testing, replace $(basename $1 | cut -d'_' -f1) with $1
    if [ $# -eq 0 ]; then
        return $FAILURE
    fi
    if dpkg-query -W -f='${Status}' $(basename $1 | cut -d'_' -f1) 2> /dev/null | grep -q "ok installed"; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Check if "apt-get update" has ever been run
function apt_repo_updated {
    if [ $(HISTTIMEFORMAT="%d/%m/%y %T " history | grep "[a]pt-get update | wc -l") -gt 0 ]; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Download a package and its dependencies; move them to a local directory
# Usage: apt_download <pkg-name>
function apt_download {
    if [ $# -eq 0 ]; then
        return $FAILURE
    fi
    sudo apt-get clean
    sudo apt-get update
    sudo apt-get --download-only --yes reinstall $1
    sudo mkdir -p $REPO_MNT_PATH/debs/$1
    sudo mv /var/cache/apt/archives/*.deb $REPO_MNT_PATH/debs/$1/
}

# Install package via apt-get if not installed
# Usage: install_apt_pkg <pkg-name>
function install_apt_pkg {
    local rtn_code=$SUCCESS
    if ! pkg_installed $1; then
        echo_blank -n "$_INSTALL Installing \"$1\"..."
        sudo apt-get -qq --yes install $1 &> /dev/null
        if ! pkg_installed $1; then
            echo_failure -e "$_INSTALL \"$1\" failed to install"
            rtn_code=$FAILURE
        else
            echo_success -e "$_INSTALL Installed  \"$1\""
        fi
    else
        echo_skip -e "$_INSTALL Skipping \"$1\"... already installed"
    fi
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

# Uninstall package via apt-get if installed
# Usage: uninstall_apt_pkg <pkg-name>
function uninstall_apt_pkg {
    local rtn_code=$SUCCESS
    if pkg_installed $1; then
        echo_blank -n "$_UNINSTALL Uninstalling \"$1\"..."
        sudo apt-get --auto-remove --purge -qq --yes remove $1 &> /dev/null
        if pkg_installed $1; then
            echo_failure -e "$_UNINSTALL \"$1\" failed to uninstall"
            rtn_code=$FAILURE
        else
            echo_success -e "$_UNINSTALL Uninstalled \"$1\""
        fi
    else
        echo_skip -e "$_UNINSTALL Skipping \"$1\"... not installed"
    fi
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

function task_update {
    echo_blank -n "$_UPDATE Updating software repositories..."
    sudo apt-get autoclean 1> /dev/null
    sudo apt-get --yes update 1> /dev/null
    echo_success -e "$_UPDATE Updated software repositories"
}

# Install package via apt-get if not installed, and configure the convironment
# if installing a specific package
# Usage: task_install <pkglist_array>
function task_install {
    local pkglist=("$@")
    local rtn_code=$SUCCESS
    for i in "${pkglist[@]}"; do
        local changed=false
        local pkgname=$i
        case $i in
            "abcde")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Override /etc/$i.conf default values for flac output
                    cat <<EOF > $HOME/.$i.conf
FLACENCODERSYNTAX=default
FLAC=flac
METAFLAC=metaflac
OUTPUTTYPE=flac
EJECTCD=y
EOF
                    changed=true
                fi
                ;;
            "apt-mirror")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $REPO_PROPERMOUNT; then
                    download_if_missing_from_mirror https updates.signal.org/desktop/apt/dists/xenial/main/binary-amd64/Packages
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/stable/main/i18n/Translation-en.bz2
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/testing/main/i18n/Translation-en.bz2
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/stable/main/Contents-all.gz
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/testing/main/Contents-all.gz
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/stable/contrib/i18n/Translation-en.bz2
                    #download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/testing/contrib/i18n/Translation-en.xz
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/stable/contrib/Contents-all.gz
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/testing/contrib/Contents-all.gz
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/stable/non-free/i18n/Translation-en.bz2
                    #download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/testing/non-free/i18n/Translation-en.xz
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/stable/non-free/Contents-all.gz
                    download_if_missing_from_mirror http ftp.us.debian.org/debian/dists/testing/non-free/Contents-all.gz
                    # These values may differ from what you want the system to use
                    local debian_components="main contrib non-free"
                    cat <<EOF | sudo tee /etc/apt/mirror.list 1> /dev/null
############# config ##################
#
# set base_path    $REPO_MNT_PATH
#
# set mirror_path  $base_path/mirror
# set skel_path    $base_path/skel
# set var_path     $base_path/var
# set cleanscript $var_path/clean.sh
# set defaultarch  <running host architecture>
# set postmirror_script $var_path/postmirror.sh
# set run_postmirror 0
set nthreads 20
set _tilde   0
#
############# end config ##############

# External repositories
deb https://packages.microsoft.com/repos/ms-teams stable main
deb http://repository.spotify.com stable non-free
deb http://repository.spotify.com testing non-free
deb https://repo.fortinet.com/repo/7.0/ubuntu bionic multiverse
deb https://updates.signal.org/desktop/apt xenial main
deb http://dl.google.com/linux/chrome/deb/ stable main

deb http://$DEBIAN_MIRROR stable $debian_components
deb http://$DEBIAN_MIRROR testing $debian_components
#deb-src http://$DEBIAN_MIRROR stable $debian_components
#deb-src http://$DEBIAN_MIRROR testing $debian_components

# mirror additional architectures
#deb-alpha http://$DEBIAN_MIRROR stable $debian_components
#deb-alpha http://$DEBIAN_MIRROR testing $debian_components
deb-amd64 http://$DEBIAN_MIRROR stable $debian_components
deb-amd64 http://$DEBIAN_MIRROR testing $debian_components
deb-arm64 http://$DEBIAN_MIRROR stable $debian_components
deb-arm64 http://$DEBIAN_MIRROR testing $debian_components
#deb-armel http://$DEBIAN_MIRROR stable $debian_components
#deb-armel http://$DEBIAN_MIRROR testing $debian_components
#deb-hppa http://$DEBIAN_MIRROR stable $debian_components
#deb-hppa http://$DEBIAN_MIRROR testing $debian_components
deb-i386 http://$DEBIAN_MIRROR stable $debian_components
deb-i386 http://$DEBIAN_MIRROR testing $debian_components
#deb-ia64 http://$DEBIAN_MIRROR stable $debian_components
#deb-ia64 http://$DEBIAN_MIRROR testing $debian_components
#deb-m68k http://$DEBIAN_MIRROR stable $debian_components
#deb-m68k http://$DEBIAN_MIRROR testing $debian_components
#deb-mips http://$DEBIAN_MIRROR stable $debian_components
#deb-mips http://$DEBIAN_MIRROR testing $debian_components
#deb-mipsel http://$DEBIAN_MIRROR stable $debian_components
#deb-mipsel http://$DEBIAN_MIRROR testing $debian_components
#deb-powerpc http://$DEBIAN_MIRROR stable $debian_components
#deb-powerpc http://$DEBIAN_MIRROR testing $debian_components
#deb-s390 http://$DEBIAN_MIRROR stable $debian_components
#deb-s390 http://$DEBIAN_MIRROR testing $debian_components
#deb-sparc http://$DEBIAN_MIRROR stable $debian_components
#deb-sparc http://$DEBIAN_MIRROR testing $debian_components

clean http://$DEBIAN_MIRROR
EOF
                    changed=true
                fi
                ;;
            "arduino")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    mkdir -p $HOME/.$i
                    cat <<EOF > $HOME/.$i/preferences.txt
board=leonardo
browser=mozilla
browser.linux=mozilla
build.verbose=true
console=true
console.auto_clear=true
console.error.file=stderr.txt
console.length=500
console.lines=4
console.output.file=stdout.txt
editor.antialias=false
editor.caret.blink=true
editor.divider.size=0
editor.divider.size.windows=2
editor.external=false
editor.font=Monospaced,plain,12
editor.font.macosx=Monaco,plain,10
editor.indent=true
editor.invalid=false
editor.keys.alternative_cut_copy_paste=true
editor.keys.alternative_cut_copy_paste.macosx=false
editor.keys.home_and_end_travel_far=false
editor.keys.home_and_end_travel_far.macosx=true
editor.keys.shift_backspace_is_delete=true
editor.languages.current=
editor.tabs.expand=true
editor.tabs.size=2
editor.update_extension=true
editor.window.height.default=600
editor.window.height.min=290
editor.window.width.default=500
editor.window.width.min=400
export.applet.separate_jar_files=false
export.application.fullscreen=false
export.application.platform=true
export.application.platform.linux=true
export.application.platform.macosx=true
export.application.platform.windows=true
export.application.stop=true
export.delete_target_folder=true\
launcher=xdg-open
platform.auto_file_type_associations=true
preproc.color_datatype=true
preproc.enhanced_casting=true
preproc.imports.list=java.applet.*,java.awt.Dimension,java.awt.Frame,java.awt.event.MouseEvent,java.awt.event.KeyEvent,java.awt.event.FocusEvent,java.awt.Image,java.io.*,java.net.*,java.text.*,java.util.*,java.util.zip.*,java.util.regex.*
preproc.output_parse_tree=false
preproc.save_build_files=false
preproc.substitute_floats=true
preproc.substitute_unicode=true
preproc.web_colors=true
programmer=arduino:avrispmkii
run.display=1
run.options=
run.options.memory=false
run.options.memory.initial=64
run.options.memory.maximum=256
run.present.bgcolor=#666666
run.present.exclusive=false
run.present.exclusive.macosx=true
run.present.stop.color=#cccccc
run.window.bgcolor=#DFDFDF
serial.databits=8
serial.debug_rate=115200
serial.line_ending=1
serial.parity=N
serial.port=/dev/ttyACM0
serial.stopbits=1
sketchbook.path=$HOME/Documents/work/sketchbook
target=arduino
update.check=true
upload.using=bootloader
upload.verbose=true
upload.verify=true
EOF
                    changed=true
                fi
                ;;
            "arp-scan")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    alias scaneth='sudo arp-scan --interface=wlp1s0 --localnet'
                    changed=true
                fi
                ;;
            "brasero")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    task_install automake
                    task_install git
                    task_install libtool
                    local destpath=/tmp/libdvdcss.tmp
                    if $REPO_PROPERMOUNT && [ -d "$REPO_MNT_PATH/libdvdcss.git" ]; then
                        git clone "$REPO_MNT_PATH/libdvdcss.git" $destpath &> /dev/null
                    elif ! $OFFLINE; then
                        git clone https://code.videolan.org/videolan/libdvdcss.git $destpath &> /dev/null
                    fi
                    pushd $destpath > /dev/null
                    make clean &> /dev/null
                    git checkout tags/1.4.3 &> /dev/null
                    autoreconf -i &> /dev/null
                    ./configure --prefix=/usr > /dev/null
                    make > /dev/null
                    sudo make install > /dev/null
                    popd > /dev/null
                    rm -rf $destpath
                    echo "REMINDER: Please restart brasero between rips to improve success rates"
                    changed=true
                fi
                ;;
            "chromium-bsu")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    cat <<EOF > $HOME/.$i
# Chromium B.S.U. configuration file
# Please read the documentation for more info
# Only modifications to option values are preserved.
use_playList 0
debug 0
full_screen 0
true_color 0
swap_stereo 0
auto_speed 0
show_fps 1
screenWidth 800
screenHeight 600
gfxLevel 2
gameSkillBase 0.5
movementSpeed 0.03
maxLevel 1
volSound 0.9
volMusic 0.5
viewGamma 1.1
audioType 0
textType 0
EOF
                    changed=true
                fi
                ;;
            "cups")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    sudo systemctl start $i
                    changed=true
                fi
                ;;
            "dmidecode")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Check if ECC memory installed
                    alias chkeccmem="sudo dmidecode --type memory | grep 'Correction Type'"

                    local product=$(sudo dmidecode -t system | grep Product)
                    case "${product##*: }" in
                        "a64")
                            # Aliases specific to the OLIMEX A64-OLinuXino

                            # Output CPU temp (A64-OLinuXino)
                            alias temp="cat /sys/devices/virtual/thermal/thermal_zone0/temp"
                            ;;
                        "librem_13v4")
                            # Aliases specific to the Purism Librem 13
                            alias temp="cat /sys/class/hwmon/hwmon0/temp1_input && cat /sys/class/hwmon/hwmon1/temp1_input"

                            # Check/set laptop monitor brightness (may not work for ext. monitor)
                            local blpath="/sys/class/backlight/intel_backlight/brightness"
                            alias bright="cat $blpath"
                            ;;
                        "teres")
                            # Aliases specific to the OLIMEX TERES-I

                            # Backlight for TERES-I only works as of Linux 5.2 kernel
                            local blpath="/sys/class/backlight/backlight/brightness"
                            alias bright="cat $blpath"
                            alias bright0="echo 0 > $blpath"
                            alias bright1="echo 1 > $blpath"
                            alias bright2="echo 2 > $blpath"
                            alias bright3="echo 3 > $blpath"
                            alias bright4="echo 4 > $blpath"
                            alias bright5="echo 5 > $blpath"
                            alias bright6="echo 6 > $blpath"
                            alias bright7="echo 7 > $blpath"
                            alias bright8="echo 8 > $blpath"
                            alias bright9="echo 9 > $blpath"
                            alias bright10="echo 10 > $blpath"
                            ;;
                    esac
                    changed=true
                fi
                ;;
            "docker.io")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    sudo systemctl start docker
                    echo "TODO: docker.io config: automatically log in to docker container registry"
                    changed=true
                fi
                ;;
            "draw.io")
                if ! install_apt_pkg $REPO_MNT_PATH/drawio-amd64-15.8.7.deb; then
                    rtn_code=$FAILURE
                fi
                ;;
            "emacs")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    cat <<EOF > $HOME/.emacs
;;(load "vhdl_emacs.el")

;;(require 'smart-tab)
;;(global-smart-tab-mode 1)
;;(require 'smooth-scroll)
;;(smooth-scroll-mode t)

(global-set-key [f5] 'goto-line)
(global-set-key [f6] 'query-replace)
(global-set-key [f10] 'hexl-insert-hex-char)

(custom-set-variables
  ;; custom-set-variables was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 '(indent-tabs-mode nil)
 '(vhdl-clock-edge-condition (quote function))
 '(vhdl-reset-active-high t)
 '(inhibit-startup-screen t))

(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
(setq column-number-mode t) ;; Enable column headers, right of line numbers
(setq c-default-style "linux" c-basic-offset 2)

;; MATLAB mode
(autoload 'matlab-mode "matlab" "MATLAB Editing Mode" t)
(add-to-list
 'auto-mode-alist
 '("\\.m$" . matlab-mode))
(setq matlab-indent-function t)
(setq matlab-shell-command "matlab")

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq-default indent-tabs-mode nil) ;; Insert spaces instead of tabs
EOF
                    changed=true
                fi
                ;;
            "evolution")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    mkdir -p $HOME/.config/$i
                    update_symlink "$HOME_MNT_PATH/.config/evolution"      $HOME/.config/evolution
                    update_symlink "$HOME_MNT_PATH/.local/share/evolution" $HOME/.local/share/evolution
                    changed=true
                fi
                ;;
            "fail2ban")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    cat <<EOF | sudo tee /etc/fail2ban/jail.conf 1> /dev/null
[DEFAULT]
ignoreip = 127.0.0.1/8 ::1
bantime = 3600
findtime = 600
maxretry = 5

[sshd]
enabled = true
EOF
                    cat <<EOF | sudo tee /etc/fail2ban/jail.d/wordpress.conf 1> /dev/null
[wordpress]
enabled = true
filter = wordpress
logpath = /var/log/auth.log
maxretry = 3
port = http,https
bantime = 300
EOF
                    sudo systemctl enable --now fail2ban &> /dev/null
                    changed=true
                fi
                ;;
            "fdupes")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    alias fdupes="fdupes -rS" # Recursive; show size
                    changed=true
                fi
                ;;
            "firefox-esr")
                # TODO: if task_install called recursively, this messes us "Configured $i" output
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    if $REPO_PROPERMOUNT; then
                        task_install $REPO_MNT_PATH/net.downloadhelper.coapp-1.6.3-1_amd64.deb
                    fi
                    update_symlink "$HOME_MNT_PATH/.mozilla"       $HOME/.mozilla
                    update_symlink "$HOME_MNT_PATH/.cache/mozilla" $HOME/.cache/mozilla
                    changed=true
                fi
                ;;
            "forticlient")
                if $REPO_PROPERMOUNT; then
                    sudo apt-get install -y \
                        $REPO_MNT_PATH/mirror/repo.fortinet.com/repo/7.0/ubuntu/pool/multiverse/forticlient/forticlient_7.0.1.0057_amd64.deb \
                        $REPO_MNT_PATH/libappindicator1_0.4.92-7_amd64.deb \
                        $REPO_MNT_PATH/libindicator7_0.5.0-4_amd64.deb \
                        &> /dev/null
                else
                    rtn_code=$FAILURE
                fi
                if $PERSONALIZE && $HOME_PROPERMOUNT; then
                    update_symlink "$HOME_MNT_PATH/.config/FortiClient" $HOME/.config/FortiClient
                    changed=true
                fi
                ;;
            "geany")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    mkdir -p $HOME/.config/$i
                    update_symlink "$HOME_MNT_PATH/.config/$i/$i.conf" $HOME/.config/$i/$i.conf
                    changed=true
                fi
                ;;
            "gedit")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    gsettings set org.gnome.gedit.preferences.ui     statusbar-visible       false
                    gsettings set org.gnome.gedit.plugins.terminal   scrollback-lines        128
                    gsettings set org.gnome.gedit.preferences.editor display-right-margin    true
                    gsettings set org.gnome.gedit.preferences.editor restore-cursor-position false
                    gsettings set org.gnome.gedit.preferences.editor bracket-matching        false
                    gsettings set org.gnome.gedit.preferences.editor scheme                  "oblivion"
                    gsettings set org.gnome.gedit.preferences.editor editor-font             "Monospace 12"
                    gsettings set org.gnome.gedit.preferences.editor insert-spaces           true
                    gsettings set org.gnome.gedit.preferences.editor tabs-size               "uint32 4"
                    gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline false
                    gsettings set org.gnome.gedit.preferences.editor auto-indent             false
                    gsettings set org.gnome.gedit.preferences.editor max-undo-actions        2048
                    changed=true
                fi
                ;;
            "git")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Alternatively create a ~/.gitconfig file
                    git config --global alias.br "branch"
                    git config --global alias.branch "branch -vv"
                    git config --global alias.ci "commit"
                    git config --global alias.co "checkout"
                    git config --global alias.deletelastcommit "reset --hard HEAD~1"
                    git config --global alias.diff-nofm "-c core.fileMode=false diff"
                    git config --global alias.fetch "fetch --all --prune"
                    git config --global alias.push "push --set-upstream origin"
                    git config --global alias.st "status --ignored"
                    git config --global alias.l "log --graph --pretty=format=\"%C(yellow)%h%C(cyan)%d%Creset %s %C(white)- %an, %ar%Creset\""
                    git config --global alias.ll "log --stat --abbrev-commit"
                    git config --global alias.gr "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) %C(bold green)%ar%C(reset) %C(white)%s%C(reset) %C(dim white)-%an%C(reset)%C(bold yellow)%d%C(reset)' --all"
                    git config --global alias.gr1 "log --graph --full-history --all --color --pretty=format:\"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s [%an]\""
                    git config --global branch.autosetuprebase "always"
                    git config --global color.branch "auto"
                    git config --global color.diff "auto"
                    git config --global color.interactive "auto"
                    git config --global color.status "auto"
                    git config --global color.ui "auto"
                    git config --global color.vi "auto"
                    git config --global core.editor "vi"
                    git config --global core.fileMode "true"
                    git config --global diff.tool "meld"
                    git config --global difftool.prompt "false"
                    #git config --global "difftool meld".cmd "meld $LOCAL $REMOTE"
                    git config --global merge.tool "meld"
                    git config --global pull.rebase false # merge (default) rather than rebase or fast-forward
                    git config --global user.email $EMAIL
                    git config --global user.name "$NAME"
                    git config --global cola.spellCheck "false"
                    changed=true
                fi
                ;;
            "gnome-boxes")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $VM_PROPERMOUNT; then
                    local boxes_images=$HOME/.local/share/gnome-boxes/images
                    local boxes_config=$HOME/.config/libvirt/qemu
                    local boxes_snapshot=$HOME/.config/libvirt/qemu/snapshot

                    local pureos_boxes_qcow2=$boxes_images/$pureos_name.$pureos_type
                    local pureos_boxes_config=$boxes_config/$pureos_name.xml
                    local pureos_boxes_snapshot="$boxes_snapshot/$pureos_name/Yet another snapshot.xml" # TODO: can't handle spaces

                    local win10_boxes_qcow2=$boxes_images/$win10_name.$win10_type
                    local win10_boxes_config=$boxes_config/$win10_name.xml
                    local win10_boxes_snapshot="$boxes_snapshot/$win10_name/Initial snapshot.xml" # TODO: can't handle spaces

                    local android_boxes_qcow2=$boxes_images/$android_name.$android_type
                    local android_boxes_config=$boxes_config/$android_name.xml

                    # gnome-boxes wants to "import" the qcow2 by copying it to the
                    # local directory. We only want to retain our existing image, so
                    # convince the system that it is already there
                    if [ -d $HOME/.local/share/gnome-boxes ]; then
                        update_symlink "$VM_MNT_PATH" $HOME/.local/share/gnome-boxes/images
                    fi
                    changed=true
                fi
                ;;
            "gnome-maps")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    gsettings set org.gnome.Maps osm-username $OSM_USERNAME
                    changed=true
                fi
                ;;
            "gnome-software")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    gsettings set org.gnome.software first-run false
                    changed=true
                fi
                ;;
            "gnome-system-monitor")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    gsettings set org.gnome.gnome-system-monitor show-dependencies true
                    changed=true
                fi
                ;;
            "gnome-weather")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    gsettings set org.gnome.Weather locations "[<(uint32 2, <('Portland', 'KPDX', true, [(0.79571014457688405, -2.1397785149603687)], [(0.7945341242735976, -2.1411037260081156)])>)>, <(uint32 2, <('San Antonio', 'KSSF', true, [(0.51206021192714668, -1.7186548090774469)], [(0.5135478084084989, -1.7190381008344775)])>)>]"
                    changed=true
                fi
                ;;
            "keepassx")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    mkdir -p $HOME/.config/$i
                    # TODO: Do not hard-code database file location
                    cat <<EOF > $HOME/.config/$i/keepassx2.ini
[General]
LastDatabases=$HOME/Documents/keepassx.kdbx
LastDir=$HOME/Documents
ShowToolbar=true
EOF
                    changed=true
                fi
                ;;
            "lshw")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Show network hardware information
                    alias netinfo='sudo lshw -class network'
                    changed=true
                fi
                ;;
            "lxmusic")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    mkdir -p $HOME/.config/$i
                    cat <<EOF > $HOME/.config/$i/config
[Main]
width=1095
height=578
xpos=0
ypos=0
show_tray_icon=1
close_to_tray=1
play_after_exit=0
show_playlist=1
filter=0
EOF
                    changed=true
                fi
                ;;
            "lxpanel")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    mkdir -p $HOME/.config/$i
                    cp -r "$REPOROOT/home/.config/$i" $HOME/.config
                    changed=true
                fi
                ;;
            "meld")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    gsettings set org.gnome.meld indent-width                  4
                    gsettings set org.gnome.meld insert-spaces-instead-of-tabs true
                    gsettings set org.gnome.meld show-line-numbers             true
                    changed=true
                fi
                ;;
            "mplayer")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    alias asciivid="mplayer -vo caca" # Watch a video via the command line
                    changed=true
                fi
                ;;
            "nautilus")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Set configurations
                    gsettings set org.gnome.nautilus.compression default-compression-format "tar.xz"
                    gsettings set org.gnome.nautilus.preferences show-hidden-files          true
                    gsettings set org.gnome.nautilus.preferences default-folder-viewer      "list-view"
                    gsettings set org.gnome.nautilus.preferences executable-text-activation "launch"
                    gsettings set org.gnome.nautilus.preferences recursive-search           "always"
                    gsettings set org.gnome.nautilus.preferences show-create-link           true
                    gsettings set org.gnome.nautilus.preferences show-delete-permanently    true
                    gsettings set org.gnome.nautilus.preferences show-directory-item-counts "always"
                    gsettings set org.gnome.nautilus.preferences show-image-thumbnails      "always"
                    #gsettings set org.gnome.nautilus.preferences sort-folders-first         true
                    gsettings set org.gnome.nautilus.list-view   default-zoom-level         "small"

                    # Add bookmarks
                    local cfgpath="$HOME/.config/gtk-3.0/bookmarks"
                    if $GAMES_PROPERMOUNT; then
                        append_unique_string_to_file "file://$GAMES_MNT_PATH" $cfgpath
                    fi
                    if $VM_PROPERMOUNT; then
                        append_unique_string_to_file "file://$VM_MNT_PATH" $cfgpath
                    fi
                    if [ -d "$NAS_ROOT" ]; then
                        append_unique_string_to_file "file://$NAS_ROOT" $cfgpath
                    fi
                    changed=true
                fi
                ;;
            "neverball")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $GAMES_PROPERMOUNT; then
                    update_symlink "$GAMES_MNT_PATH/video-game-saves/save/pc/Neverball" $HOME/.neverball
                    changed=true
                fi
                ;;
            "nextcloud-desktop")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    update_symlink "$HOME_MNT_PATH/Nextcloud" $HOME/Nextcloud
                    mkdir -p $HOME/.config/autostart
                    cat <<EOF > $HOME/.config/autostart/com.nextcloud.desktopclient.nextcloud.desktop
[Desktop Entry]
Name=Nextcloud
GenericName=File Synchronizer
Exec="/usr/bin/nextcloud" --background
Terminal=false
Icon=Nextcloud
Categories=Network
Type=Application
StartupNotify=false
X-GNOME-Autostart-enabled=true
EOF
                    changed=true
                fi
                ;;
            "nfs-common")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    if mount_nas; then
                        changed=true
                    else
                        echo_skip -e "$_CONFIG Not mounting NAS... network unavailable when configuring $i"
                        rtn_code=$FAILURE
                    fi
                fi
                ;;
            "octave")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    mkdir -p $HOME/.config/$i
                    cp -r "$REPOROOT/home/.config/$i" $HOME/.config
                    changed=true
                fi
                ;;
            "open-iscsi")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                fi
                sudo systemctl start iscsid
                sudo iscsiadm --mode discovery -t sendtargets --portal $IP_NAS_ETH:3260 1> /dev/null
                sudo iscsiadm --mode node --targetname iqn.1994-11.com.netgear:readynas:ff5a9ec6:games --portal $IP_NAS_ETH:3260 --login 1> /dev/null
                changed=true
                ;;
            "openbox")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    cat <<EOF > $HOME/.dmrc
[Desktop]
Language=en_US.utf8
Session=openbox
EOF
                    changed=true
                fi
                ;;
            "openmw")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $GAMES_PROPERMOUNT; then
                    update_symlink "$GAMES_MNT_PATH/Morrowind" /usr/games/Morrowind sudo
                    update_symlink "$GAMES_MNT_PATH/video-game-saves/save/pc/The Elder Scrolls III: Morrowind/openmw/config" $HOME/.config/openmw
                    update_symlink "$GAMES_MNT_PATH/video-game-saves/save/pc/The Elder Scrolls III: Morrowind/openmw/saves" $HOME/.local/share/openmw/saves
                    changed=true
                fi
                ;;
            "openssh-server")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    sudo systemctl enable --now ssh &> /dev/null
                    changed=true
                fi
                ;;
            "openvpn")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    update_symlink "$HOME_MNT_PATH/.cert" $HOME/.cert
                    changed=true
                fi
                ;;
            "pidgin")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    update_symlink "$HOME_MNT_PATH/.purple" $HOME/.purple
                    changed=true
                fi
                ;;
            "playonlinux")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $GAMES_PROPERMOUNT; then
                    update_symlink "$GAMES_MNT_PATH/.PlayOnLinux" $HOME/.PlayOnLinux
                    update_symlink "$GAMES_MNT_PATH/.PlayOnLinux//wineprefix/" "$HOME/PlayOnLinux's virtual drives"
                fi
                ;;
            "python3")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    cat <<EOF > $HOME/.pythonrc
# ~/.pythonrc
# enable syntax completion
try:
    import readline
except ImportError:
    print("Module readline not available.")
else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")
EOF
                    export PYTHONSTARTUP=$HOME/.pythonrc # Used for python auto-completion
                    changed=true
                fi
                ;;
            "python3-pip")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $INTERNET_CONNECTED; then
                    sudo pip3 install pyconnman > /dev/null
                    sudo pip3 install pywinusb > /dev/null
                fi
                ;;
            "rdesktop")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Remote desktop with compression and local sound
                    alias rdesktop="rdesktop -k en-us -f -N -z -rsound:local"
                    changed=true
                fi
                ;;
            "rednotebook")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    update_symlink "$HOME_MNT_PATH/.$i" $HOME/.$i
                    cat <<EOF > $HOME/.$i/configuration.cfg
autoSwitchMode=0
checkForNewVersion=0
closeToTray=0
cloudIgnoreList=filter, these, comma, separated, words, and, #tags, katelyn
cloudIncludeList=mtv, spam, work, job, play
cloudMaxTags=1000
dataDir=$HOME/.rednotebook/data
dateTimeString=%A, %x %X
exportDateFormat=%A, %x
firstStart=0
instantSearch=1
lastBackupDate=9999-12-31
leftDividerPosition=253
mainFont=Sans 9
mainFrameHeight=1016
mainFrameMaximized=0
mainFrameWidth=1393
mainFrameX=26
mainFrameY=23
previewFont=mingliu, MS Mincho, sans-serif
rightDividerPosition=705
showTagsPane=0
spellcheck=0
useInternalPreview=1
userDir=
weekNumbers=0
EOF
                    changed=true
                fi
                ;;
            "rsync")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    alias rsync='rsync -chavzP --append-verify --stats' # Copy files with stats
                    alias rsync-local='rsync --archive --checksum --delete --force --partial --progress --stats --verbose' # don't need to compress, etc, files when xferring locally
                    changed=true
                fi
                ;;
            "samba")
                # TODO: Share doesn't currently appear locally or in Windows
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    local localip=$(hostname -I | cut -d' ' -f1)
                    local smbpath="/etc/samba/smb.conf"
                    append_unique_string_to_file "[gamesaverepo]" $smbpath sudo
                    append_unique_string_to_file "comment = Git repo for video game saves" $smbpath sudo
                    append_unique_string_to_file "guest ok = yes" $smbpath sudo
                    append_unique_string_to_file "hosts allow = 127.0.0.1/8 ${localip%.*}.0/24" $smbpath sudo
                    append_unique_string_to_file "path = $GAMES_MNT_PATH/video-game-saves" $smbpath sudo
                    append_unique_string_to_file "browseable = yes" $smbpath sudo #TODO: this won't be placed because it's not unique in the file
                    append_unique_string_to_file "read only = no" $smbpath sudo
                    sudo systemctl restart smbd
                    changed=true
                fi
                ;;
            "scrcpy")
                if ! install_apt_pkg android-sdk-platform-tools; then
                    rtn_code=$FAILURE
                elif ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif ! test_connection p2; then
                    echo_failure -e "Unable to detect phone p2 on local network $ACTIVE_SSID"
                    rtn_code=$FAILURE
                else
                    #adb start-server > /dev/null # for USB-connected devices
                    if ! adb devices | grep -q 2233; then
                        if adb tcpip 2233 > /dev/null; then # for wireless connection
                            read -p "About to issue phone prompt; press enter to continue"
                            adb connect p2:2233 > /dev/null
                            read -p "Accept phone prompt, then press enter"
                        else
                            rtn_code=$FAILURE
                        fi
                    fi
                    changed=true
                fi
                ;;
            "signal-desktop")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                fi
                if $PERSONALIZE && $HOME_PROPERMOUNT; then
                    update_symlink "$HOME_MNT_PATH/.config/Signal" $HOME/.config/Signal
                    changed=true
                fi
                ;;
            "snapd")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    sudo systemctl start snapd
                    changed=true
                fi
                ;;
            "steam")
                add_arch i386
                if $SW_UPDATE; then
                    task_update
                fi

                # can't use install_apt_pkg due to TUI prompts
                sudo apt-get -qq --yes install $i

                if pkg_installed $i; then
                    echo_blank -n "$_CONFIG Configuring $i "
                    if [ -d $GAMES_MNT_PATH ]; then
                        update_symlink "$GAMES_MNT_PATH/.steam" $HOME/.steam
                        update_symlink "$GAMES_MNT_PATH/.steam/sdk32/steam" $HOME/.steampath
                        update_symlink "$GAMES_MNT_PATH/.steam/steam.pid" $HOME/.steampid
                        update_symlink "$GAMES_MNT_PATH/.wine" $HOME/.wine
                    else
                        echo_failure -e "\"$GAMES_MNT_PATH\" not found"
                    fi
                    echo_success -e "$_CONFIG Configured $i"
                    if $NAS_EXISTS; then
                        echo_blank -n "$_CONFIG Configuring iSCSI "
                        mount_scsi $IP_NAS_ETH \
                                   3260 \
                                   "iqn.1994-11.com.netgear:readynas:ff5a9ec6:games" \
                                   $STEAM_LUN_PARTITION_LABEL \
                                   $STEAM_LUN_MNT_PATH
                        echo_success -e "$_CONFIG Configured iSCSI"
                    fi
                else
                    echo_failure -e "$_CONFIG Failed to configure $i... package not installed"
                    rtn_code=$FAILURE
                fi
                ;;
            "teams")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT; then
                    update_symlink "$HOME_MNT_PATH/.config/Microsoft" $HOME/.config/Microsoft
                    changed=true
                fi
                ;;
            "transmission")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    mkdir -p $HOME/.config/$i
                    cp -r "$REPOROOT/home/.config/$i" $HOME/.config
                    changed=true
                fi
                ;;
            "tiemu")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    cp -r "$REPOROOT/home/.tiemu" $HOME
                    changed=true
                fi
                ;;
            "trigger-rally")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $GAMES_PROPERMOUNT; then
                    update_symlink "$PC_GAMESAVE_MNT_PATH/Trigger Rally" $HOME/.trigger-rally
                    changed=true
                fi
                ;;
            "tzdata")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Configure time zone
                    sudo ln -fs /usr/share/zoneinfo/$TIME_ZONE /etc/localtime
                    sudo dpkg-reconfigure --frontend=noninteractive tzdata &> /dev/null
                    changed=true
                fi
                ;;
            "ufw")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    sudo ufw limit 22/tcp 1> /dev/null
                    sudo ufw allow 80/tcp 1> /dev/null
                    sudo ufw allow 443/tcp 1> /dev/null
                    sudo ufw default deny incoming 1> /dev/null
                    sudo ufw default allow outgoing 1> /dev/null
                    sudo ufw enable 1> /dev/null
                    changed=true
                fi
                ;;
            "upower")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # Display battery information
                    #alias battery='upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep -E "state|time\ to\ empty|time\ to\ full|percentage"'
                    alias battery='upower -i $(upower -e | grep "BAT")'
                    changed=true
                fi
                ;;
            "vim")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    cat <<EOF > $HOME/.vimrc
" .vimrc

set backspace=indent,eol,start

" c: automatically wrap comments
" q: allow rewrapping of comments with gq
" r: automatically insert comment character
" l: don't break previously long lines
" set formatoptions=cqrlo

" General
set ts=4                       " ts=tabstop; Size of a hard tabstop
set sw=4                       " sw=shiftwidth; Size of an indent
set softtabstop=4              " A combination of spaces and tabs are used to simulate tab stops at a width other than the (hard) tabstop
set cin
set et
set is
set list                       " Enables tab character replacement for visibility
set listchars=tab:>-           " Replace "tab" char with ">---"
set nu
set smarttab                   " Make 'tab' insert indents instead of tabs at the beginning of a line
set expandtab                  " Always use spaces instead of tabs
" set tw=80                      " tw=textwidth; Set the default text width. This enters a carriage return when the max width is reached.

" Folding
" set foldmethod=syntax
" let c_no_comment_fold = 1    " Uncomment to stop comments from folding
" let c_no_if0_fold = 1        " Uncomment to stop ifs from folding

" Backups
if has("vms")
    set nobackup
else
    set backup
    set backupdir=~/.vimbackups,~/tmp/,/tmp/
    set backupcopy=yes
    set viewdir=$VIMHOME/views/
endif

" Syntax highlighting, highlight last search
if &t_Co > 2 || has("gui_running")
    colorscheme desert
    syntax on
    set hlsearch
endif

" In many terminal emulators the mouse works just find, thus enable it.
if v:version >= 700
    set mouse=a
endif

" This is only for tabs.  It will nuber the tabs and report how many windows
" are in each as well as rename them better
" if has("gui")
"     set guioptions-=e
" endif
if exists("+showtabline")
    function! MyTabLine()
        let s = ''
        let t = tabpagenr()
        let last = tabpagenr('$')
        let groupsize = 4
        let i = 1
        let max = t/groupsize + 1
        let max = max * groupsize
        let tabsepleft='['
        let tabsepright=']'
        while i <= tabpagenr('$')
            let buflist = tabpagebuflist(i)
            let winnr = tabpagewinnr(i)
            let s .= '%' . i . 'T'
            let s .= (i == t ? '%1*' : '%2*')
            let s .= ' '
            let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
            let s .= tabsepleft . i . ':'
            let bufnr = buflist[winnr - 1]
            let file = bufname(bufnr)
            let buftype = getbufvar(bufnr, 'buftype')
            if buftype == 'nofile'
                if file =~ '\/.'
                    let file = substitute(file, '.*\/\ze.', '', '')
                endif
            else
                let file = fnamemodify(file, ':p:t')
            endif
            if file == ''
                let file = '[No Name]'
            endif
            let s .= file
            let s .= tabsepright
            let i = i + 1
            if i > max
                break
            endif
        endwhile
        let s .= "%T%#TabLineFill#%="
        let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
        return s
    endfunction

    set stal=2
    set tabline=%!MyTabLine()
    hi TablineSel ctermbg=blue ctermfg=white
    hi TabLineFill ctermbg=white ctermfg=black
endif
EOF
                    changed=true
                fi
                ;;
            "virt-manager")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    # Traverse VM list and link ones with an existing storage file
                    vm_init
                    if $VM_PROPERMOUNT; then
                        link_all_vm
                    else
                        echo_failure "$_CONFIG VMs not linked... partition not mounted"
                    fi

                    # Configure virt-manager
                    gsettings set org.virt-manager.virt-manager         system-tray true
                    gsettings set org.virt-manager.virt-manager.console scaling     2
                    changed=true
                fi
                ;;
            "visualboyadvance")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $GAMES_PROPERMOUNT; then
                    update_symlink "$GAMES_MNT_PATH/video-game-saves/cfg/VisualBoyAdvance.cfg" $HOME/.VisualBoyAdvance.cfg
                    changed=true
                fi
                ;;
            "vlc")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    # TODO: This will overwrite any changes from other applications;
                    # TODO: Play well with others
                    cat <<EOF > $HOME/.config/mimeapps.list
[Default Applications]
application/x-cd-image=vlc.desktop
video/mp4=vlc.desktop
video/x-matroska=vlc.desktop

[Added Associations]
application/x-cd-image=vlc.desktop;
video/mp4=vlc.desktop;
video/x-matroska=vlc.desktop;
EOF
                    changed=true
                fi
                ;;
            "waydroid")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $HOME_PROPERMOUNT && $VM_PROPERMOUNT; then
                    mkdir -p $HOME/.local/share/waydroid/data/media/0
                    update_symlink "$HOME_MNT_PATH/.local/share/waydroid/data/media/0" $HOME/.local/share/waydroid/data/media/0
                    sudo mkdir -p /var/lib/waydroid/images
                    update_symlink "$VM_MNT_PATH/waydroid" /var/lib/waydroid sudo
                    # TODO: waydroid app currently must be run in GUI mode to select "GAPPS"
                    # TODO: rather than "VANILLA" prior to running init command
                    # sudo waydroid init # TODO: check if destructive with existing installation
                    sudo systemctl enable --now waydroid-container
                    changed=true
                fi
                ;;
            "wget")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    cat <<EOF > $HOME/.wgetrc
adjust_extension = on
convert_links = on
mirror = on
page_requisites = on
span_hosts = on
timestamping = on
verbose = off
EOF
                    changed=true
                fi
                ;;
            "wine")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $GAMES_PROPERMOUNT; then
                    update_symlink "$GAMES_MNT_PATH/.wine" $HOME/.wine
                fi
                ;;
            "wine32")
                add_arch i386
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE && $GAMES_PROPERMOUNT; then
                    update_symlink "$GAMES_MNT_PATH/.wine" $HOME/.wine
                fi
                ;;
            "xournal")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    mkdir -p $HOME/.xournal
                    cp "$REPOROOT/home/.xournal/config" $HOME/.xournal/config
                    changed=true
                fi
                ;;
            "youtube-dl")
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif $PERSONALIZE; then
                    if $NAS_PROPERMOUNT; then
                        local prep="sudo"
                        local outputroot="$NAS_ROOT/internet/youtube.com"
                        rmdir "$HOME/Downloads/youtube-dl" 2> /dev/null # Created during installation
                    else
                        local prep=""
                        local outputroot="$HOME/Downloads/youtube-dl"
                    fi
                    $prep mkdir -p $outputroot
                    local cfgpath=$HOME/.config/$i
                    mkdir -p "$cfgpath"
                    cat <<EOF > $cfgpath/config
## General
--ignore-errors                                       # Skip unavailable videos on a playlist

## Filesystem
--batch-file $outputroot/youtube-dl.urls              # List of videos to download

# Download all playlists of YouTube channel/user keeping each playlist in separate directory:
# --output '%(uploader)s/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s'
--output '$outputroot/%(uploader)s/%(title)s.%(ext)s' # Download video under directory of uploader

--no-overwrites                                       # Do not overwrite files
--continue                                            # Resume partial files if possible
--write-description                                   # Write metadata to a .description file
--write-info-json                                     # Write metadata to a .info.json file
--write-annotations                                   # Create an .annotations.xml file
--cookies $outputroot/youtube.com_cookies.txt         # Use Chrome's "Get Cookies" extension

## Thumbnail
--write-all-thumbnails                                # Download all thumbnail image formats

## Video format
# Download best webm (video-only+audio-only) format available
# OR       best webm video+audio format available
# OR       any other best if nothing previous available
--format 'bestvideo[vcodec=vp9]+bestaudio[acodec=opus]/best[ext=webm]/best'

# youtube-dl --list-formats <video-url> # List available formats

## Subtitle
--all-subs                                            # Download all available subtitles

## Post-processing
--no-post-overwrites                                  # Do not overwrite post-processed files
--embed-subs                                          # Embed subtitles in mp4/webm/mkv
--embed-thumbnail                                     # Embed thumbnail in audio as cover art
--add-metadata                                        # Write metadata to the video file

# To purge youtube-dl cache (if it won't redownload a file because it exists):
# youtube-dl --rm-cache-dir --ignore-config

## References
# https://github.com/ytdl-org/youtube-dl#format-selection
EOF
                    changed=true
                fi
                ;;
            *)
                if ! install_apt_pkg $i; then
                    rtn_code=$FAILURE
                fi
                ;;
        esac
        if $changed; then
            # Use pkgname rather than 'i', as 'i' may get overwritten inside the loop
            echo_success -e "$_CONFIG Configured \"${pkgname//\*}\""
        fi
    done
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

# Uninstall software
function task_uninstall {
    local pkglist=("$@")
    local rtn_code=$SUCCESS
    for i in "${pkglist[@]}"; do
        local changed=false
        case "${i//\*}" in # remove * chars
            "calamares-settings-debian")
                if ! uninstall_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    rm -f $HOME/Desktop/install-debian.desktop
                    changed=true
                fi
                ;;
            "mozc")
                if ! uninstall_apt_pkg $i; then
                    rtn_code=$FAILURE
                else
                    if [ -d $HOME/.config/$i ]; then
                        rm -rf $HOME/.config/$i
                        changed=true
                    fi
                    if [ -f $HOME/.config/ibus-mozc-gnome-initial-setup-done ]; then
                        rm $HOME/.config/ibus-mozc-gnome-initial-setup-done
                        changed=true
                    fi
                fi
                ;;
            "yelp")
                if ! uninstall_apt_pkg $i; then
                    rtn_code=$FAILURE
                elif [ -d $HOME/.config/$i ]; then
                    rm -rf $HOME/.config/$i
                    changed=true
                fi
                ;;
            *)
                if ! uninstall_apt_pkg $i; then
                    rtn_code=$FAILURE
                fi
                ;;
        esac
        if $changed; then
            echo_success -e "$_CONFIG Cleaned \"${i//\*}\""
        fi
    done
    if [ $rtn_code == $SUCCESS ]; then return $SUCCESS; else return $FAILURE; fi
}

function task_upgrade {
    sudo apt-get --yes upgrade &> /dev/null
}

function task_dist_upgrade {
    sudo apt-get --yes dist-upgrade &> /dev/null
}

# Usage: get_signing_key <key-url> <keyring-dest> <deb-url> <components> <list-path>
function get_signing_key {
    if [ $# -ne 5 ]; then
        echo "ERROR: Insufficient number of parameters provided to get_signing_key"
        return $FAILURE
    fi
    local key_url="$1"
    local keyring_dest="/usr/share/keyrings/$2"
    local deb_url="$3"
    local components="$4"
    local list_path="/etc/apt/sources.list.d/$5"
    local sign_path="signed-by=$keyring_dest"

    # TODO: create signing key in repo, if missing AND able to download
    #wget -O - https://$key_url | sudo tee $REPO_MNT_PATH/signing-keys/...

    if [ -f "$REPO_MNT_PATH/signing-keys/$key_url" ]; then
        sudo cp "$REPO_MNT_PATH/signing-keys/$key_url" "$keyring_dest"
    elif ! $OFFLINE && pkg_installed curl; then
        curl -sS https://$key_url | gpg --dearmor | sudo tee "$keyring_dest" 1> /dev/null
    else
        echo_failure "$_CONFIG Unable to locate \"$key_url\" signing key and curl is not installed"
    fi
    if [ -n "$deb_url" -a -n "$list_path" ]; then
        echo "deb [arch=$ARCH $sign_path] $deb_url $components" | sudo tee "$list_path" 1> /dev/null
    fi


    if $REPO_WANTMOUNT && $REPO_PROPERMOUNT; then
        # comment all lines
        sudo sed -i 's/^#*/#/' "$list_path"

        # remove http/https: from the url (now looks like url with preceding //)
        local deb_url_cropped=$(echo $deb_url | cut -d":" -f2)

        # prepend line to sources.list so local repo takes precedence against
        # external packages matching same name+version
        sudo sed -i "1i deb [arch=$ARCH $sign_path trusted=yes] file:$REPO_MNT_PATH/mirror/${deb_url_cropped:2} $components" $list_path
    fi
}

# Requires: curl
function task_apt_sources {

    # `curl -sS` is functionally equivalent to `wget -O-`

    echo_blank -n "$_CONFIG Including \"$COMPONENTS\" in \"/etc/apt/sources.list\"..."

    # Official Debian sources
    cat <<EOF | sudo tee /etc/apt/sources.list 1> /dev/null
deb http://deb.debian.org/debian $SUITE $COMPONENTS
deb http://deb.debian.org/debian $SUITE/updates $COMPONENTS
deb http://security.debian.org/debian-security/ $SUITE-security $COMPONENTS
EOF

    # FortiClient VPN
    get_signing_key \
        "repo.fortinet.com/repo/7.0/ubuntu/DEB-GPG-KEY" \
        "fortinet.gpg" \
        "https://repo.fortinet.com/repo/7.0/ubuntu" \
        "bionic multiverse" \
        "forticlient-vpn.list"

    # Google Chrome
    get_signing_key \
        "dl-ssl.google.com/linux/linux_signing_key.pub" \
        "linux_signing_key.pub" \
        "http://dl.google.com/linux/chrome/deb" \
        "stable main" \
        "google-chrome.list"

    # Microsoft Teams
    get_signing_key \
        "packages.microsoft.com/keys/microsoft.asc" \
        "microsoft.gpg" \
        "https://packages.microsoft.com/repos/ms-teams" \
        "stable main" \
        "teams.list"

    # Signal
    get_signing_key \
        "updates.signal.org/desktop/apt/keys.asc" \
        "signal-desktop-keyring.gpg" \
        "https://updates.signal.org/desktop/apt" \
        "xenial main" \
        "signal-xenial.list"

    # Spotify
    get_signing_key \
        "download.spotify.com/debian/pubkey_0D811D58.gpg" \
        "spotify_debian_pubkey_0D811D58.gpg" \
        "http://repository.spotify.com" \
        "stable non-free" \
        "spotify.list"

    # Waydroid
    get_signing_key \
        "repo.waydro.id/waydroid.gpg" \
        "waydroid.gpg" \
        "https://repo.waydro.id/" \
        "bullseye main" \
        "waydroid.list"

    # Zoom
    # TODO: Doesn't work yet
    #curl -sS https://zoom.us/linux/download/pubkey | gpg --dearmor | sudo tee /usr/share/keyrings/zoom.gpg 1> /dev/null
    #sudo curl --url https://zoom.us/client/latest/zoom_$ARCH.deb --output $REPO_MNT_PATH/zoom_$ARCH.deb

    # TODO: The repo can be mounted but not at /var/spool/apt-mirror
    if $REPO_WANTMOUNT && $REPO_PROPERMOUNT; then
        sudo sed -i 's/^#*/#/' /etc/apt/sources.list
        sudo sed -i "1i deb file:$REPO_MNT_PATH/mirror/$DEBIAN_MIRROR $SUITE $COMPONENTS" /etc/apt/sources.list
    fi
    echo_success -e "$_CONFIG \"$COMPONENTS\" repos written to \"/etc/apt/sources.list\""
}
