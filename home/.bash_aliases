alias ..='cd ..'                                                 # Change working directory up 1 level
alias ...='cd ../..'                                             # Change working directory up 2 levels
alias ....='cd ../../..'                                         # Change working directory up 3 levels
alias .....='cd ../../../..'                                     # Change working directory up 4 levels
alias ......='cd ../../../../..'                                 # Change working directory up 5 levels
alias biggestpkg="dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -n | tail -n 10"
#alias cls="printf '\033\143'"                                    # Clear terminal and scrollback history
alias cp="cp -pr"                                                # Preserve file modification date, etc, when copying
alias date="date +%Y%m%d_%H%M%S"                                 # Display date in an easily-sortable format
alias df="df -h --sync --total"                                  # Show filesystem capacities, mount locations
alias extip="host myip.opendns.com resolver1.opendns.com | grep 'has address' | cut -d' ' -f4"        # Print external IP address
alias emptytrash='rm -rf ~/.local/share/Trash/*'                 # Empty the trash
alias follow='namei -mo'                                         # Recurse down each level of a directory path, displaying permissions
alias gitreposhere="find . -type d -exec test -e '{}/.git' ';' -print -prune"
alias intip='hostname -I'                                        # Print internal IP address
alias l="ls -CF"
alias la='ls -ahlF --color=auto --group-directories-first'       # Nicely print working directory contents
alias ll='ls -hlF  --color=auto --group-directories-first'       # Nicely print working directory contents
alias ls='ls -h    --color=auto --group-directories-first'       # Nicely print working directory contents
alias please="sudo"                                              # Make the terminal a little more pleasant
alias screenusb='sudo screen /dev/ttyUSB0 115200'                # Use the screen device to connect to a USB-attached device
alias services='sudo systemctl list-unit-files --type=service'   # List all services on a systemd-based device
alias sizeofpwd='du -ch | grep total'                            # Display the size on disk of the present working directory

#alias temp="/opt/vc/bin/vcgencmd measure_temp"                   # Output CPU temp

# Package administration
alias purgerc='dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs sudo dpkg --purge' # Remove all packaged marked 'rc'

# Wi-Fi operations and information
alias netrestart='sudo systemctl restart networking'      # Reboot network services
alias scanwifi='sudo iw dev wlp1s0 scan | grep SSID:'     # Print the SSIDs of wireless networks available. TODO: Genericize interface name.

# Add an "alert" alias for long running commands.  Use like so: "sleep 10; alert"
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Colors
if [ `which dircolors` ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto --group-directories-first'
    alias grep='grep --color=auto'
fi

# #!/bin/bash -e
# alias make="make ${@} 2>&1 | perl -wln -M'Term::ANSIColor' -e ' m/Building|gcc|g++|\bCC\b|\bcc\b/ and print "\e[1;32m", "$_", "\e[0m" or m/Error/i and print "\e[1;91m", "$_", "\e[0m" or m/Warning/i and print "\e[1;93m", "$_", "\e[0m" or m/Linking|\.a\b/ and print "\e[1;36m", "$_", "\e[0m" or print; '"
