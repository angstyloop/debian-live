# Perform all normal update procedures
#
cuua() {
    if [ "$#" -gt 1 ]; then
        echo "CRITICAL: Illegal number of parameters"
        echo "USAGE: cuua <-q>"
        return
    fi
    if [ "$1" == "-q" ]; then
        echo "NOT UPGRADING ANY PACKAGES HELD BACK!"
        echo -en "Cleaning repositories\t\t"
        sudo apt-get clean > /dev/null
        echo -en "done\n"
        echo -en "Updating repositories\t\t"
        sudo apt-get update > /dev/null
        echo -en "done\n"
        echo -en "Upgrading software\t\t"
        sudo apt-get -y upgrade > /dev/null
        echo -en "done\n"
        echo -en "Removing unused software\t"
        sudo apt-get -y autoremove > /dev/null
        echo -en "done\n"
        echo -en "Purging packages marked 'rc'\t"
        if [ $(( dpkg -l | grep "^rc" | wc -l )) -gt 0 ]; then
            dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs sudo dpkg --purge
        fi
        echo -en "done\n"
    else
        echo "NOT UPGRADING ANY PACKAGES HELD BACK!"
        sudo apt-get clean
        sudo apt-get update
        sudo apt-get -y upgrade
        sudo apt-get -y autoremove
        if [ $((`dpkg -l | grep "^rc" | wc -l`)) -gt 0 ]; then
            dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs sudo dpkg --purge
        fi
    fi
}

# Mount a filesystem image file (*.img) into a desired directory
#
mntimg() {
    if [ "$#" -ne 2 ]; then
        echo "CRITICAL: Illegal number of parameters"
        echo "USAGE: mntimg <file.img> </path/to/mnt/dir/>"
        return
    fi
    if [ ! -f "$1" ]; then
        echo "CRITICAL: File \"$1\" not found!"
        return
    fi
    if [ ! -d "$2" ]; then
        echo "WARNING: Directory \"$2\" does not exist!"
        while true; do
            read -p "Do you wish to create this directory?" yn
            case $yn in
                [Yy]* ) sudo mkdir -p "$2"; break;;
                [Nn]* ) echo "Nothing else to do. Exiting."; return;;
                * ) echo "Please answer yes or no.";;
            esac
        done
    fi
    block_size=`sudo fdisk -l "$1" | grep physical | cut -d ' ' -f 7`
    start_block=`sudo fdisk -l "$1" | grep "$1" | grep -v Disk | cut -d ' ' -f 8`
    echo "block size:  $block_size"
    echo "start block: $start_block"
    if [ "$start_block" == "" ]; then
        echo "Detected single partition"
        start_block=0
    else
        echo "Detected full disk image"
    fi
    echo 'sudo mount -o loop,offset="$(($block_size * $start_block))" "$1" "$2";'
    # sudo mount -o loop,offset="$(($block_size * $start_block))" "$1" "$2";
}

# Count the number of directories within a given directory
#
numdirsin() {
    if [ "$#" -ne 1 ]; then
        echo "CRITICAL: Illegal number of parameters"
        echo "USAGE: numdirsin(<directory>)"
        return
    fi
    if [ ! -d "$1" ]; then
        echo "CRITICAL: Directory \"$1\" not found!"
        return
    fi
    ls -la $1 | grep -c ^d
    ls -la $1 | grep -c .^d
}

# Count the number of files within a given directory
#
numfilesin() {
    if [ "$#" -ne 1 ]; then
        echo "CRITICAL: Illegal number of parameters"
        echo "USAGE: numdirsin(<directory>)"
        return
    fi
    if [ ! -d "$1" ]; then
        echo "CRITICAL: Directory \"$1\" not found!"
        return
    fi
    ls -la $1 | grep -c ^f
    ls -la $1 | grep -c .^f
}

# Pull all git repos under a particular directory
pullall() {
    IFS=$'\n'; for dir in $(gitreposhere); do pushd $dir > /dev/null; git fetch --all --prune; git pull; popd > /dev/null; done
}
