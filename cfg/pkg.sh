#!/bin/bash

function read_pkg_lists {
    ############################################################################
    # Package lists

    #INSTALL_WITH_NOTIFY_LIST=(\
    #    libdvdcss2 \
    #    popularity-contest \
    #    wireshark \
    #)

    #UNINSTALL_WITH_NOTIFY_LIST=(\
    #    dictionaries-common \
    #)

    # General install list for packages NOT requiring a graphical environment
    CLI_INSTALL_LIST=(\
        btrfs-progs \
        cifs-utils \
        dmidecode \
        e2fsprogs \
        fail2ban \
        fdupes \
        git \
        gpg \
        kpcli \
        mutt \
        ncdu \
        net-tools \
        nfs-common \
        ntfs-3g \
        openssh-client \
        openssh-server \
        openvpn \
        pdftk \
        rsync \
        samba \
        tree \
        tzdata \
        udisks2 \
        ufw \
        vrms \
        xz-utils \
    )

    DESIGN_INSTALL_LIST=(\
        ardour \
        asciidoc \
        audacity \
        blender \
        bluefish \
        darktable \
        dia \
        freecad \
        fritzing \
        gimp \
        gnome-paint \
        godot3 \
        kicad \
        krita \
        inkscape \
        librecad \
        lmms \
        musescore \
        openscad \
        openshot \
        rawtherapee \
        shotwell \
        slic3r \
        sweethome3d* \
        texlive-fonts-extra \
        texlive-latex-extra \
        texlive-latex-recommended \
        texstudio \
    )

    DEV_INSTALL_LIST=(\
        adb \
        aircrack-ng \
        android-sdk \
        apt-file \
        arachne-pnr \
        arduino \
        arp-scan \
        automake \
        avr-libc \
        baobab \
        bc \
        bison \
        build-essential \
        chrpath \
        cmake \
        cmake-curses-gui \
        cmake-doc \
        cpio \
        dconf-editor \
        dfu-programmer \
        dia \
        diffutils \
        diffstat \
        dosfstools \
        doxygen \
        draw.io \
        fakeroot \
        flex \
        forticlient \
        g++-arm-linux-gnueabihf \
        gawk \
        gcc \
        gcc-aarch64-linux-gnu \
        gcc-arm-linux-gnueabihf \
        gcc-arm-none-eabi \
        gcc-avr \
        geany \
        gnuradio \
        gparted \
        gtimelog \
        gtkwave \
        hdparm \
        josm \
        kmod \
        libelf-dev \
        libncurses5-dev \
        libnewlib-arm-none-eabi \
        libssl-dev \
        libtool \
        lshw \
        meld \
        mkdocs \
        neofetch \
        nmap \
        octave \
        pylint \
        pylint3 \
        python3 \
        python3-aiofiles \
        python3-alsaaudio \
        python3-git \
        python3-hid \
        python3-libgpiod \
        python3-pip \
        python3-pydantic \
        python3-pyelftools \
        python3-serial-asyncio \
        python3-wget \
        python3-wheel \
        screen \
        shellcheck \
        tiemu \
        tmux \
        traceroute \
        u-boot-tools \
        valgrind \
        vim \
        xxd \
        yosys \
        yosys-dev \
        yosys-doc \
    )

    #mgba
    GAME_INSTALL_LIST=(\
        0ad \
        aisleriot \
        briquolo \
        chromium-bsu \
        cool-retro-term \
        dosbox \
        freeciv \
        freedink \
        fretsonfire \
        gnome-games-app \
        gnome-sudoku \
        lutris \
        mame \
        manaplus \
        marsshooter \
        minetest \
        minetest-mod* \
        mupen64plus-qt \
        nestopia \
        openarena \
        openmw \
        openttd \
        osmose-emulator \
        pcsxr \
        pcsx2 \
        scummvm \
        stella \
        stellarium \
        supertuxkart \
        trigger-rally \
        tuxguitar \
        vcmi \
        visualboyadvance \
        wesnoth \
        wine \
        wine32 \
        zsnes \
    )

    MEDIA_INSTALL_LIST=(\
        abcde \
        atomicparsley \
        audacity \
        brasero \
        btrfs-progs \
        calibre \
        cmus \
        easytag \
        flac \
        flactag \
        gthumb \
        guvcview \
        handbrake \
        iat \
        libimage-exiftool-perl \
        lollypop \
        mkvtoolnix-gui \
        mplayer \
        obs-studio \
        openshot \
        qv4l2 \
        smtube \
        spotify-client \
        streamlink \
        transmission \
        v4l-utils \
        youtube-dl \
    )

    #MISC_INSTALL_LIST=(\
    #    gnucash \
    #    klavaro \
    #)

    # google chrome is required to use video capabilities for dialpad
    SOCIAL_INSTALL_LIST=(\
        abook \
        empathy \
        google-chrome-stable \
        pidgin \
        polari \
        revolt \
        teams \
    )

    VM_INSTALL_LIST=(\
        anbox \
        qemu-system-arm \
        gnome-boxes \
        ovmf \
        virt-manager \
        waydroid \
    )

    VM_HEADLESS_INSTALL_LIST=(\
        bridge-utils \
        libvirt-clients \
        libvirt-daemon-system \
        qemu \
        qemu-system \
        qemu-utils \
        virtinst \
    )

    #evolution # TODO: full backup works but not symlinking the 2 dirs
    #python
    # General install list for packages requiring a graphical environment
    DESKTOP_INSTALL_LIST=(\
        clamtk \
        cups \
        ffmpegthumbnailer \
        file-roller \
        firefox-esr \
        galternatives \
        gedit \
        gedit-plugins \
        gnome-authenticator \
        gnome-calendar \
        gnome-control-center \
        gnome-disk-utility \
        gnome-maps \
        gnome-power-manager \
        gnome-software \
        gnome-software-plugin-flatpak \
        gnome-system-monitor \
        gnome-todo \
        gnome-weather \
        keepassx \
        nautilus \
        nextcloud-desktop \
        ooo-thumbnailer \
        pcmanfm \
        playonlinux \
        rdesktop \
        rednotebook \
        remmina \
        scrcpy \
        seahorse \
        software-properties-gtk \
        syncthing-gtk \
        system-config-printer \
        tilix \
        vlc \
        xournal \
        xserver-xorg-core \
        xserver-xorg-video-intel \
    )

    SERVER_INSTALL_LIST=(\
        openssh-server \
        ntfs-3g \
        open-iscsi \
        nfs-common \
        ufw \
    )

    UNINSTALL_LIST=(\
        anthy* \
        aspell-he \
        calamares-settings-debian \
        clipit \
        culmus \
        debian-reference-* \
        deluge* \
        fcitx* \
        firefox-esr-l10n* \
        gnome-accessibility-themes \
        gnome-games \
        gnome-music \
        gnome-photos \
        gnome-terminal \
        gnome-themes-extra \
        goldendict \
        gucharmap \
        im-config \
        libreoffice-l10n* \
        live-task-localisation \
        live-task-localisation-desktop \
        lxmusic \
        menu \
        mlterm* \
        mozc* \
        mpv \
        myspell-he \
        orca \
        plymouth \
        rhythmbox \
        smplayer \
        synaptic \
        task-hebrew-desktop \
        task-hebrew-gnome-desktop \
        thunderbird \
        totem \
        uim* \
        user-setup \
        xiterm+thai \
        xsane \
        xterm \
        yelp* \
    )
}
