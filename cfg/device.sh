#!/bin/bash
# Depends: util/apt.sh
# Depends: util/device.sh
# Depends: util/fs.sh

# Check if the block device of a partition exists
# Depends: dev_check_paths
# Depends: util/fs.sh - path_exists
function dev_check_exists {
    path_exists "$GAMES_PARTITION_DEV"     GAMES_EXISTS
    path_exists "$HOME_PARTITION_DEV"      HOME_EXISTS
    path_exists "$REPO_PARTITION_DEV"      REPO_EXISTS
    path_exists "$STEAM_LUN_PARTITION_DEV" STEAM_LUN_EXISTS
    path_exists "$VM_PARTITION_DEV"        VM_EXISTS
}

function dev_check_if_nbd {
    NBD_REQUESTED=false
    # nbd must be enabled if *any* virtual filesystems are to be mounted
    if [ -n "$HOME_VIRT_PATH" -o -n "$REPO_VIRT_PATH" -o -n "$GAMES_VIRT_PATH" ]; then
        NBD_REQUESTED=true
    fi
}

function dev_check_paths {
    if [ -f "$GAMES_VIRT_PATH" -a -n "$GAMES_NBD" ]; then
        GAMES_PARTITION_DEV=$GAMES_NBD
    else
        GAMES_PARTITION_DEV=$(get_device_path_from_label $GAMES_PARTITION_LABEL 2> /dev/null)
    fi
    if [ -f "$HOME_VIRT_PATH" -a -n "$HOME_NBD" ]; then
        HOME_PARTITION_DEV=$HOME_NBD
    else
        HOME_PARTITION_DEV=$(get_device_path_from_label $HOME_PARTITION_LABEL 2> /dev/null)
    fi
    if [ -f "$REPO_VIRT_PATH" -a -n "$REPO_NBD" ]; then
        REPO_PARTITION_DEV=$REPO_NBD
    else
        REPO_PARTITION_DEV=$(get_device_path_from_label $REPO_PARTITION_LABEL 2> /dev/null)
    fi
    STEAM_LUN_PARTITION_DEV=$(get_device_path_from_label $STEAM_LUN_PARTITION_LABEL 2> /dev/null)
    VM_PARTITION_DEV=$(get_device_path_from_label $VM_PARTITION_LABEL 2> /dev/null)
}

# Depends: dev_check_exists
function dev_check_mounted {
    if $GAMES_EXISTS     && dev_mounted $GAMES_PARTITION_DEV; then     GAMES_MOUNTED=true; else     GAMES_MOUNTED=false; fi
    if $HOME_EXISTS      && dev_mounted $HOME_PARTITION_DEV; then      HOME_MOUNTED=true; else      HOME_MOUNTED=false;  fi
    if $REPO_EXISTS      && dev_mounted $REPO_PARTITION_DEV; then      REPO_MOUNTED=true; else      REPO_MOUNTED=false;  fi
    if $STEAM_LUN_EXISTS && dev_mounted $STEAM_LUN_PARTITION_DEV; then STEAM_LUN_MOUNTED=true; else STEAM_LUN_MOUNTED=false; fi
    if $VM_EXISTS        && dev_mounted $VM_PARTITION_DEV; then        VM_MOUNTED=true; else        VM_MOUNTED=false;    fi
    if dev_mounted $IP_NAS_ETH:/data/Audiobooks && \
       dev_mounted $IP_NAS_ETH:/data/Fitness    && \
       dev_mounted $IP_NAS_ETH:/data/GitLab     && \
       dev_mounted $IP_NAS_ETH:/data/Internet   && \
       dev_mounted $IP_NAS_ETH:/data/Movies     && \
       dev_mounted $IP_NAS_ETH:/data/Music      && \
       dev_mounted $IP_NAS_ETH:/data/Other      && \
       dev_mounted $IP_NAS_ETH:/data/Television
    then
        NAS_MOUNTED=true
    else
        NAS_MOUNTED=false
    fi
}

# Check if devices are mounted at the desired path
# Usage: dev_check_properly_mounted <device_path>
function dev_check_properly_mounted {
    # TODO: The additional checks for $*_EXISTS is required but is messy;
    #       there's got to be a better approach.
    GAMES_PROPERMOUNT=false
    if $GAMES_EXISTS && verify_mount_path $GAMES_PARTITION_DEV $GAMES_MNT_PATH; then GAMES_PROPERMOUNT=true; fi
    HOME_PROPERMOUNT=false
    if $HOME_EXISTS && verify_mount_path $HOME_PARTITION_DEV $HOME_MNT_PATH; then HOME_PROPERMOUNT=true; fi
    REPO_PROPERMOUNT=false
    if $REPO_EXISTS && verify_mount_path $REPO_PARTITION_DEV $REPO_MNT_PATH; then REPO_PROPERMOUNT=true; fi
    STEAM_LUN_PROPERMOUNT=false
    if $STEAM_LUN_EXISTS && verify_mount_path $STEAM_LUN_PARTITION_DEV $STEAM_LUN_MNT_PATH; then STEAM_LUN_PROPERMOUNT=true; fi
    VM_PROPERMOUNT=false
    if $VM_EXISTS && verify_mount_path $VM_PARTITION_DEV $VM_MNT_PATH; then VM_PROPERMOUNT=true; fi

    NAS_PROPERMOUNT=true
    # Iterate through lower-case share names and see if all the paths exist
    for i in "${NAS_SHARE_LIST[@]/#/\/mnt/nas/}"; do # TODO: use /mnt/nas as global variable NAS_ROOT
        if [ ! -d "${i,,}" ]; then NAS_PROPERMOUNT=false; fi
    done
}

# Describe the cases in which each of the devices are mounted
function dev_check_want_mounted {
    GAMES_WANTMOUNT=false
    HOME_WANTMOUNT=false
    NAS_WANTMOUNT=false
    REPO_WANTMOUNT=false
    STEAM_LUN_WANTMOUNT=false
    VM_WANTMOUNT=false

    if $SW_INSTALL_GAMES || $SW_INSTALL_STEAM; then
        GAMES_WANTMOUNT=true
    fi

    if $PERSISTENT_HOME; then
        HOME_WANTMOUNT=true
    fi

    if $NAS_EXISTS; then
        NAS_WANTMOUNT=true
    fi

    if $REPO_EXISTS && ( $OFFLINE || $NAS_EXISTS || ( ! $OFFLINE && $METERED_NETWORK ) ); then
        REPO_WANTMOUNT=true
    fi

    if $SW_INSTALL_STEAM; then
        STEAM_LUN_WANTMOUNT=true
    fi

    if $SW_INSTALL_VM; then
        VM_WANTMOUNT=true
    fi
}

# Connect a Network Block Device (nbd) if a virtual filesystem is provided
# Depends: util/apt.sh
function dev_connect_if_nbd {
    if [ -f "$HOME_VIRT_PATH" ]; then  nbd_connect $HOME_NBD $HOME_VIRT_PATH;  fi
    if [ -f "$REPO_VIRT_PATH" ]; then  nbd_connect $REPO_NBD $REPO_VIRT_PATH;  fi
    if [ -f "$GAMES_VIRT_PATH" ]; then nbd_connect $GAMES_NBD $GAMES_VIRT_PATH; fi
}

# Mount devices
# Depends: util/device.sh
# Depends: util/fs.sh
function dev_mount {
    if $REPO_WANTMOUNT; then
        mount_if_not_mounted $REPO_PARTITION_DEV $REPO_MNT_PATH

        # Not running apt-mirror, but this is where we placed the packages/lists
        update_symlink $REPO_MNT_PATH/var-lib-apt-lists /var/lib/apt/lists sudo
    fi
    # TODO: Organize this better
    # return to a folder if the symlink breaks for some reason
    if is_a_broken_symlink /var/lib/apt/lists; then
        sudo rm /var/lib/apt/lists
        sudo mkdir /var/lib/apt/lists
    fi

    if $HOME_WANTMOUNT; then
        mount_if_not_mounted $HOME_PARTITION_DEV $HOME_MNT_PATH
    fi
    if $GAMES_WANTMOUNT; then
        mount_if_not_mounted $GAMES_PARTITION_DEV $GAMES_MNT_PATH
    fi
    if $STEAM_LUN_WANTMOUNT; then
        mount_if_not_mounted $STEAM_LUN_PARTITION_DEV $STEAM_LUN_MNT_PATH
    fi
    if $VM_WANTMOUNT; then
        mount_if_not_mounted $VM_PARTITION_DEV $VM_MNT_PATH
    fi
}

# Link personal directories into ones located on persistent storage
function task_persistent_home {
    # TODO: Consider cloning and symlinking env-config/home.git as an alternative to symlinking individual folders
    if $HOME_PROPERMOUNT; then
        update_symlink $HOME_MNT_PATH/Desktop        $HOME/Desktop
        update_symlink $HOME_MNT_PATH/Documents      $HOME/Documents
        update_symlink $HOME_MNT_PATH/Downloads      $HOME/Downloads
        update_symlink $HOME_MNT_PATH/Music          $HOME/Music
        update_symlink $HOME_MNT_PATH/Pictures       $HOME/Pictures
        update_symlink $HOME_MNT_PATH/Public         $HOME/Public
        update_symlink $HOME_MNT_PATH/Templates      $HOME/Templates
        update_symlink $HOME_MNT_PATH/Videos         $HOME/Videos
        update_symlink $HOME_MNT_PATH/.vnc           $HOME/.vnc
        update_symlink $HOME_MNT_PATH/.cache/spotify $HOME/.cache/spotify

        if $NAS_PROPERMOUNT; then
            # TODO: Doesn't seem to work yet; may not have this var at this point
            update_symlink "/run/$USER/1000/gvfs/dav:host=$NEXTCLOUD_DOMAIN,ssl=true,user=$NEXTCLOUD_USER,prefix=%2Fnextcloud%2Fremote.php%2Fwebdav" $HOME/Nextcloud.webdav
        fi
    fi

    if $GAMES_PROPERMOUNT; then
        update_symlink $GAMES_MNT_PATH $HOME/Games
    fi
}