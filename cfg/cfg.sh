#!/bin/bash

# This script ONLY sets values. Please do not include any real logic in here.

function load_hw_cfg {

    ################################################################################
    # User configuration

    # If providing PLATFORM as an input argument
    #if [ $# -ne 1 ]; then return $FAILURE; fi
    #case $1 in
    case $PLATFORM in
        "desktop"|"laptop")
            PRESET=basic

            # TODO: What if you provide a qcow2 path rather than a partition label?
            # TODO: What if /media/user/home already exists and the device is mounted at /media/user/home1?
            HOME_PARTITION_LABEL=home
            HOME_MNT_PATH=/media/$USER/$HOME_PARTITION_LABEL
            HOME_VIRT_PATH="" # Virtual filesystem path; leave empty if not wanted
            HOME_NBD=/dev/nbd0

            # apt-mirror resources are expected to exist here
            REPO_PARTITION_LABEL=repo
            REPO_MNT_PATH=/var/spool/apt-mirror
            REPO_VIRT_PATH="" # Virtual filesystem path; leave empty if not wanted
            REPO_NBD=/dev/nbd1

            # local steam games are expected to exist here
            GAMES_PARTITION_LABEL=Games
            GAMES_MNT_PATH=/media/$USER/$GAMES_PARTITION_LABEL
            GAMES_VIRT_PATH="" # Virtual filesystem path; leave empty if not wanted
            GAMES_NBD=/dev/nbd2

            # path to a game save repository, if applicable
            PC_GAMESAVE_MNT_PATH="" #"$GAMES_MNT_PATH/video-game-saves/save/pc"

            NAS_ROOT=/mnt/nas
            # Enter each NAS share name on a separate line; end with a backslash (ex: Audiobooks \)
            NAS_SHARE_LIST=(\
            )

            # steam games on the NAS are expected to exist here
            STEAM_LUN_PARTITION_LABEL=Steam
            STEAM_LUN_MNT_PATH=$NAS_ROOT/${STEAM_LUN_PARTITION_LABEL,,}
            # A virtual filesystem containing a network LUN doesn't make sense, so leaving out

            # QCOW2 image files are expected to exist here
            VM_PARTITION_LABEL=VM
            VM_MNT_PATH=/media/$USER/$VM_PARTITION_LABEL
            # A virtual filesystem containing VM images doesn't make sense, so leaving out

            SWAPFILEPATH=$VM_MNT_PATH/swap.file
            SWAPFILESIZE=32 # GB

            # Personalizations
            BRIGHTNESS=0.35
            DESKTOP_BACKGROUND_THEME="none"
            DESKTOP_BACKGROUND_PRIMARY="#000000"
            DESKTOP_BACKGROUND_SECONDARY="#000000"
            DISPLAY_RES="1920 1080 60" # TODO: Unused
            DOMAIN="domain"
            EMAIL="my@$DOMAIN"
            NAME="My Name"
            NEXTCLOUD_DOMAIN=$DOMAIN
            NEXTCLOUD_USER="user"
            NIGHT_LIGHT=true
            OSM_USERNAME="MyUsername"
            THEME="Adwaita-dark" # For list: ls -d /usr/share/themes/* | xargs -L 1 basename
            TIME_ZONE="America/Los_Angeles" # timedatectl list-timezones

            # Networking
            WPA_SUPPLICANT_CFG="/etc/wpa_supplicant/wpa_supplicant.conf"
            ;;
        "server")
            PRESET=server

            NAS_ROOT=/mnt/nas
            # Enter each NAS share name on a separate line; end with a backslash (ex: Audiobooks \)
            NAS_SHARE_LIST=(\
            )

            STEAM_LUN_PARTITION_LABEL=iSCSI
            STEAM_LUN_MNT_PATH=$NAS_ROOT/${STEAM_LUN_PARTITION_LABEL,,}

            VM_PARTITION_LABEL=VM
            VM_MNT_PATH=/var/lib/libvirt/images

            SWAPFILEPATH=$VM_MNT_PATH/swap.file
            SWAPFILESIZE=32 #GB
            ;;
        *)
            echo "FAILURE: Unsupported platform '$PLATFORM'"
            exit
            ;;
    esac
}

# Depends: load_hw_cfg
function load_preset {
    case $PRESET in
        "basic")
            SUITE="stable"
            COMPONENTS="main contrib non-free"
            ADD_HOSTS=true                     # add hosts to /etc/hosts
            CREATE_SWAPFILE=false              # create a swapfile
            FORCE_OFFLINE=false                # force script to use local resources
            SET_METERING=false                 # set metered network flag
            SW_UPDATE=true                     # needed to install software locally
            SW_INSTALL=true                    # needed to install *any* software
            SW_INSTALL_DESIGN=false            # install design software
            SW_INSTALL_DEV=false               # install development software
            SW_INSTALL_DOCKER=false            # install docker.io
            SW_INSTALL_KEYS=true               # install additional software repos/keys
            SW_INSTALL_GAMES=false             # install game software
            SW_INSTALL_MEDIA=false             # install audio, graphic, video tools
            SW_INSTALL_MIRROR=false            #
            SW_INSTALL_SOCIAL=false            # install chat applications
            SW_INSTALL_STEAM=false             # install Steam environment
            SW_INSTALL_VM=false                # install VM tools
            SW_PURGE=true                      # completely remove installed software
            SW_UPGRADE=true                    # run apt-get update
            USE_BLUETOOTH=false                # whether to enable Bluetooth
            PERSISTENT_HOME=true               # create symlinks for persistent storage
            PERSONALIZE=true                   # customize environment
            USE_WIFI=true                      # whether to enable WiFi
            VM_AUTOSTART=false                 # autostart VMs after linking
            VM_HEADLESS=false                  # headless mode for VM server configurations
            ;;
        "minimal")
            SUITE="stable"
            COMPONENTS="main contrib non-free"
            ADD_HOSTS=false                    # add hosts to /etc/hosts
            CREATE_SWAPFILE=false              # create a swapfile
            FORCE_OFFLINE=false                # force script to use local resources
            SET_METERING=false                 # set metered network flag
            SW_UPDATE=false                    # needed to install software locally
            SW_INSTALL=false                   # needed to install *any* software
            SW_INSTALL_DESIGN=false            # install design software
            SW_INSTALL_DEV=false               # install development software
            SW_INSTALL_DOCKER=false            # install docker.io
            SW_INSTALL_KEYS=false              # install additional software repos/keys
            SW_INSTALL_GAMES=false             # install game software
            SW_INSTALL_MEDIA=false             # install audio, graphic, video tools
            SW_INSTALL_MIRROR=false            #
            SW_INSTALL_SOCIAL=false            # install chat applications
            SW_INSTALL_STEAM=false             # install Steam environment
            SW_INSTALL_VM=false                # install VM tools
            SW_PURGE=false                     # completely remove installed software
            SW_UPGRADE=false                   # run apt-get update
            USE_BLUETOOTH=false                # whether to enable Bluetooth
            PERSISTENT_HOME=true               # create symlinks for persistent storage
            PERSONALIZE=true                   # customize environment
            USE_WIFI=false                     # whether to enable WiFi
            VM_AUTOSTART=false                 # autostart VMs after linking
            VM_HEADLESS=false                  # headless mode for VM server configurations
            ;;
        "nothing")
            SUITE="stable"
            COMPONENTS="main"
            ADD_HOSTS=false                    # add hosts to /etc/hosts
            CREATE_SWAPFILE=false              # create a swapfile
            FORCE_OFFLINE=false                # force script to use local resources
            SET_METERING=false                 # set metered network flag
            SW_UPDATE=false                    # needed to install software locally
            SW_INSTALL=false                   # needed to install *any* software
            SW_INSTALL_DESIGN=false            # install design software
            SW_INSTALL_DEV=false               # install development software
            SW_INSTALL_DOCKER=false            # install docker.io
            SW_INSTALL_KEYS=false              # install additional software repos/keys
            SW_INSTALL_GAMES=false             # install game software
            SW_INSTALL_MEDIA=false             # install audio, graphic, video tools
            SW_INSTALL_MIRROR=false            #
            SW_INSTALL_SOCIAL=false            # install chat applications
            SW_INSTALL_STEAM=false             # install Steam environment
            SW_INSTALL_VM=false                # install VM tools
            SW_PURGE=false                     # completely remove installed software
            SW_UPGRADE=false                   # run apt-get update
            USE_BLUETOOTH=false                # whether to enable Bluetooth
            PERSISTENT_HOME=false              # create symlinks for persistent storage
            PERSONALIZE=false                  # customize environment
            USE_WIFI=false                     # whether to enable WiFi
            VM_AUTOSTART=false                 # autostart VMs after linking
            VM_HEADLESS=false                  # headless mode for VM server configurations
            ;;
        "everything")
            SUITE="testing"
            COMPONENTS="main contrib non-free"
            ADD_HOSTS=true                     # add hosts to /etc/hosts
            CREATE_SWAPFILE=false              # create a swapfile
            FORCE_OFFLINE=false                # force script to use local resources
            SET_METERING=true                  # set metered network flag
            SW_UPDATE=true                     # needed to install software locally
            SW_INSTALL=true                    # needed to install *any* software
            SW_INSTALL_DESIGN=true             # install design software
            SW_INSTALL_DEV=true                # install development software
            SW_INSTALL_DOCKER=false            # install docker.io
            SW_INSTALL_KEYS=true               # install additional software repos/keys
            SW_INSTALL_GAMES=true              # install game software
            SW_INSTALL_MEDIA=true              # install audio, graphic, video tools
            SW_INSTALL_MIRROR=true             #
            SW_INSTALL_SOCIAL=true             # install chat applications
            SW_INSTALL_STEAM=true              # install Steam environment
            SW_INSTALL_VM=true                 # install VM tools
            SW_PURGE=true                      # completely remove installed software
            SW_UPGRADE=true                    # run apt-get update
            USE_BLUETOOTH=true                 # whether to enable Bluetooth
            PERSISTENT_HOME=true               # create symlinks for persistent storage
            PERSONALIZE=true                   # customize environment
            USE_WIFI=true                      # whether to enable WiFi
            VM_AUTOSTART=false                 # autostart VMs after linking
            VM_HEADLESS=false                  # headless mode for VM server configurations
            ;;
        "server")
            SUITE="stable"
            COMPONENTS="main"
            ADD_HOSTS=true                     # add hosts to /etc/hosts
            CREATE_SWAPFILE=true               # create a swapfile
            FORCE_OFFLINE=false                # force script to use local resources
            SET_METERING=false                 # set metered network flag
            SW_UPDATE=true                     # needed to install software locally
            SW_INSTALL=true                    # needed to install *any* software
            SW_INSTALL_DESIGN=false            # install design software
            SW_INSTALL_DEV=false               # install development software
            SW_INSTALL_DOCKER=false            # install docker.io
            SW_INSTALL_KEYS=false              # install additional software repos/keys
            SW_INSTALL_GAMES=false             # install game software
            SW_INSTALL_MEDIA=false             # install audio, graphic, video tools
            SW_INSTALL_MIRROR=false            #
            SW_INSTALL_SOCIAL=false            # install chat applications
            SW_INSTALL_STEAM=false             # install Steam environment
            SW_INSTALL_VM=true                 # install VM tools
            SW_PURGE=false                     # completely remove installed software
            SW_UPGRADE=true                    # run apt-get update
            USE_BLUETOOTH=false                # whether to enable Bluetooth
            PERSISTENT_HOME=false              # create symlinks for persistent storage
            PERSONALIZE=false                  # customize environment
            USE_WIFI=false                     # whether to enable WiFi
            VM_AUTOSTART=true                  # autostart VMs after linking
            VM_HEADLESS=true                   # headless mode for VM server configurations
            ;;
        "work")
            SUITE="stable"
            COMPONENTS="main contrib non-free"
            ADD_HOSTS=false                    # add hosts to /etc/hosts
            CREATE_SWAPFILE=false              # create a swapfile
            FORCE_OFFLINE=false                # force script to use local resources
            SET_METERING=false                 # set metered network flag
            SW_UPDATE=true                     # needed to install software locally
            SW_INSTALL=true                    # needed to install *any* software
            SW_INSTALL_DESIGN=false            # install design software
            SW_INSTALL_DEV=true                # install development software
            SW_INSTALL_DOCKER=false            # install docker.io
            SW_INSTALL_KEYS=false              # install additional software repos/keys
            SW_INSTALL_GAMES=false             # install game software
            SW_INSTALL_MEDIA=false             # install audio, graphic, video tools
            SW_INSTALL_MIRROR=false            #
            SW_INSTALL_SOCIAL=false            # install chat applications
            SW_INSTALL_STEAM=false             # install Steam environment
            SW_INSTALL_VM=false                # install VM tools
            SW_PURGE=true                      # completely remove installed software
            SW_UPGRADE=true                    # run apt-get update
            USE_BLUETOOTH=false                # whether to enable Bluetooth
            PERSISTENT_HOME=true               # create symlinks for persistent storage
            PERSONALIZE=true                   # customize environment
            USE_WIFI=true                      # whether to enable WiFi
            VM_AUTOSTART=false                 # autostart VMs after linking
            VM_HEADLESS=false                  # headless mode for VM server configurations
            ;;
        *)
            echo "FAILURE: Unsupported preset value '$PRESET'"
            exit
            ;;
    esac
}
