#!/bin/bash

# Depends on cfg/cfg.sh

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

# Configure general environment
# TODO (tweaks):
#   Extension
#   - Alternatetab enable
function task_personalize {

    # Set full name
    sudo chfn -f "$NAME" $USER

    # Set screen resolution
    #xrandr -s $DISPLAY_RES

    # Set screen brightness
    # TODO: Doesn't work as expected; seems to set max brightness slider to X%, not X%/100%
    #xrandr --output $(xrandr | grep " connected" | cut -d' ' -f1) --brightness $BRIGHTNESS

    # GSETTINGS TUTORIAL
    # List schemas:          gsettings list-schemas
    # List keys of schema:   gsettings list-keys <schema>
    # Get/Set key of schema: gsettings <get|set> <schema> <key>

    gsettings set org.gnome.desktop.background picture-options         $DESKTOP_BACKGROUND_THEME
    gsettings set org.gnome.desktop.background primary-color           $DESKTOP_BACKGROUND_PRIMARY
    gsettings set org.gnome.desktop.background secondary-color         $DESKTOP_BACKGROUND_SECONDARY
    gsettings set org.gnome.desktop.interface  clock-format            "24h"
    gsettings set org.gnome.desktop.interface  clock-show-weekday      true
    gsettings set org.gnome.desktop.interface  gtk-theme               $THEME
    gsettings set org.gnome.desktop.interface  show-battery-percentage true

    # Set night light
    gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled $NIGHT_LIGHT

    # Tap to click
    gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true

    # Configure favorites bar
    gsettings set org.gnome.shell favorite-apps "['firefox-esr.desktop', 'org.gnome.Nautilus.desktop', 'com.gexperts.Tilix.desktop', 'org.gnome.gedit.desktop', 'com.nextcloud.desktopclient.nextcloud.desktop', 'org.gnome.Evolution.desktop', 'org.gnome.Calendar.desktop', 'org.gnome.Calculator.desktop', 'gnome-system-monitor.desktop', 'spotify.desktop', 'pidgin.desktop', 'xournal.desktop', 'pavucontrol.desktop']"

    # Configure application list
    gsettings set org.gnome.desktop.app-folders folder-children "['Development', 'Gaming', 'Internet', 'Media', 'Office', 'Social', 'Utilities']"

    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Development/ name "Development"
    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Development/ apps "['bluefish.desktop', 'dia.desktop', 'freecad.desktop', 'fritzing.desktop', 'godot3.desktop', 'librecad.desktop', 'openscad.desktop', 'vim.desktop', 'texdoctk.desktop', 'texstudio.desktop', 'geany.desktop', 'gnuradio-grc.desktop', 'gtkwave.desktop', 'org.octave.Octave.desktop', 'org.gnome.meld.desktop', 'info.desktop', 'org.openstreetmap.josm.desktop', 'klavaro.desktop']"

    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Gaming/ name "Gaming"
    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Gaming/ apps "['steam.desktop', '0ad.desktop', 'dolphin-emu.desktop', 'sol.desktop', 'dosbox.desktop', 'org.freeciv.gtk322.desktop', 'org.freeciv.server.desktop', 'freedink-dfarc.desktop', 'freedink.desktop', 'freedinkedit.desktop', 'org.gnome.Games.desktop', 'org.gnome.Sudoku.desktop', 'net.lutris.Lutris.desktop', 'mame.desktop', 'manaplus.desktop', 'net.minetest.minetest.desktop', 'mupen64plus-qt.desktop', 'nestopia.desktop', 'org.openmw.launcher.desktop', 'openttd.desktop', 'osmose-emulator.desktop', 'pcsxr.desktop', 'scummvm.desktop', 'stella.desktop', 'supertuxkart.desktop', 'trigger-rally.desktop', 'vcmilauncher.desktop', 'vcmiclient.desktop', 'PlayOnLinux.desktop']"

    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Internet/ name "Internet"
    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Internet/ apps "['firefox-esr.desktop']"

    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Media/ name "Media"
    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Media/ apps "['ardour.desktop', 'audacity.desktop', 'blender.desktop', 'org.gnome.Cheese.desktop', 'darktable.desktop', 'easytag.desktop', 'gimp.desktop', 'shotwell.desktop', 'gnome-paint.desktop', 'org.kde.krita.desktop', 'org.inkscape.Inkscape.desktop', 'lmms.desktop', 'mscore.desktop', 'xjadeo.desktop', 'zynaddsubfx-alsa.desktop', 'zynaddsubfx-jack.desktop', 'zynaddsubfx-jack-multi.desktop', 'zynaddsubfx-oss.desktop', 'org.openshot.OpenShot.desktop', 'org.gnome.Totem.desktop', 'rawtherapee.desktop', 'com.obsproject.Studio.desktop', 'smtube.desktop', 'vlc.desktop', 'spotify.desktop', 'display-im6.q16.desktop', 'brasero.desktop', 'calibre-gui.desktop', 'fr.handbrake.ghb.desktop', 'org.gnome.Lollypop.desktop', 'qv4l2.desktop', 'timidity.desktop']"

    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Office/ name "Office"
    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Office/ apps "['org.gnome.gedit.desktop', 'org.gnome.Todo.desktop', 'org.gnome.Evolution.desktop', 'org.gnome.Contacts.desktop', 'org.gnome.Documents.desktop', 'libreoffice-startcenter.desktop', 'libreoffice-calc.desktop', 'libreoffice-draw.desktop', 'libreoffice-impress.desktop', 'libreoffice-writer.desktop', 'xournal.desktop', 'gtimelog.desktop', 'gnucash.desktop', 'rednotebook.desktop', 'com.nextcloud.desktopclient.nextcloud.desktop']"

    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Social/ name "Social"
    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Social/ apps "['teams.desktop', 'org.gnome.Polari.desktop', 'empathy.desktop', 'org.perezdecastro.Revolt.desktop', 'pidgin.desktop']"

    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Utilities/ name "Utilities"
    gsettings set org.gnome.desktop.app-folders.folder:/org/gnome/desktop/app-folders/folders/Utilities/ apps "['org.gnome.Nautilus.desktop', 'org.gnome.Calendar.desktop', 'gnome-system-monitor.desktop', 'gucharmap.desktop', 'org.gnome.Calculator.desktop', 'org.gnome.Dictionary.desktop', 'org.gnome.DiskUtility.desktop', 'org.gnome.eog.desktop', 'org.gnome.Evince.desktop', 'org.gnome.FileRoller.desktop', 'org.gnome.Screenshot.desktop', 'org.gnome.seahorse.Application.desktop', 'org.gnome.tweaks.desktop', 'simple-scan.desktop', 'org.gnome.clocks.desktop', 'org.gnome.Extensions.desktop', 'org.gnome.Firmware.desktop', 'anbox.desktop', 'clamtk.desktop', 'org.debian.galternatives.desktop', 'remote-viewer.desktop', 'org.freedesktop.MalcontentControl.desktop', 'gnome-control-center.desktop', 'org.gnome.Software.desktop', 'software-properties-gnome.desktop', 'org.gnome.SoundRecorder.desktop', 'ca.desrt.dconf-editor.desktop', 'gparted.desktop', 'org.gnome.Maps.desktop', 'com.github.bilelmoussaoui.Authenticator.desktop', 'transmission-gtk.desktop', 'org.gnome.Weather.desktop', 'org.gnome.PowerStats.desktop', 'keepassx.desktop', 'mutt.desktop', 'pcmanfm.desktop', 'system-config-printer.desktop', 'org.remmina.Remmina.desktop', 'itweb-settings.desktop', 'syncthing-start.desktop', 'syncthing-gtk.desktop', 'syncthing-ui.desktop', 'xdot.desktop', 'org.gnome.Boxes.desktop', 'virt-manager.desktop', 'winetricks.desktop', 'calibre-ebook-edit.desktop', 'calibre-lrfviewer.desktop', 'calibre-ebook-viewer.desktop', 'lstopo.desktop']"


    # Configure power settings
    gsettings set org.gnome.settings-daemon.plugins.power idle-dim                       true
    gsettings set org.gnome.settings-daemon.plugins.power idle-brightness                10
    gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-type         "nothing"
    gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout      1200
    gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-type    "nothing"
    gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 1200
    gsettings set org.gnome.settings-daemon.plugins.power power-button-action            "nothing"
    gsettings set org.gnome.settings-daemon.plugins.power ambient-enabled                true

    # Do nothing if the laptop lid is closed
    # (this is implemented in systemd and not gsettings)
    sudo sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/g' /etc/systemd/logind.conf
    sudo sed -i 's/#HandleLidSwitchExternalPower=suspend/HandleLidSwitchExternalPower=ignore/g' /etc/systemd/logind.conf

    cp "$SCRIPT_DIR/../home/.bash_aliases" $HOME/.bash_aliases
    cp "$SCRIPT_DIR/../home/.bash_functions" $HOME/.bash_functions
    cp "$SCRIPT_DIR/../home/.bash_logout" $HOME/.bash_logout
    cp "$SCRIPT_DIR/../home/.bash_profile" $HOME/.bash_profile
    cp "$SCRIPT_DIR/../home/.bashrc" $HOME/.bashrc
    #source $HOME/.bashrc # TODO: Doesn't run in anticipated shell
}