#!/bin/bash

# Depends on util/vm.sh

function link_all_vm {

    ############################################################################
    # Android
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="androidx86" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="linux" \
            --os-variant="generic" \
            --ram="4096" \
            --title="Android_x86" \
            --vcpus="$(nproc)"

    ############################################################################
    # CentOS
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="CentOS-7" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="linux" \
            --os-variant="centos7.0" \
            --ram="2048" \
            --title="CentOS_7" \
            --vcpus="$(nproc)"

    ############################################################################
    # Debian
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="Debian-10-Server" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="linux" \
            --os-variant="debian10" \
            --ram="2048" \
            --title="Debian_10_(Server)" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="PureOS VM" \
            --diskinterface="virtio" \
            --diskname="pureos" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="linux" \
            --os-variant="debian10" \
            --ram="4096" \
            --title="PureOS" \
            --vcpus="$(nproc)"

    vm_link --arch="aarch64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="debian-11-openstack-arm64" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="linux" \
            --os-variant="debiantesting" \
            --ram="2048" \
            --title="Debian10ARM" \
            --vcpus="$(nproc)"

    ############################################################################
    # Windows
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="?-bit" \
            --diskinterface="ide" \
            --diskname="Windows-XP" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="windows" \
            --os-variant="winxp" \
            --ram="1024" \
            --title="Windows_XP" \
            --vcpus="1"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="Windows-7-Pro-64bit" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="windows" \
            --os-variant="win7" \
            --ram="2048" \
            --title="Windows_7_Pro" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="Windows-10-Pro-64bit" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="windows" \
            --os-variant="win10" \
            --ram="2048" \
            --title="Windows_10_Pro" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="win10" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="windows" \
            --os-variant="win10" \
            --ram="4096" \
            --title="Windows_10" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="uefi" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="win10.uefi" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="windows" \
            --os-variant="win10" \
            --ram="4096" \
            --title="Windows_10_(UEFI)" \
            --vcpus="$(nproc)"

    ############################################################################
    # macOS
    #
    # For working macOS xml import, please refer to https://github.com/foxlet/macOS-Simple-KVM
    # TODO: Configure CPU topology, NIC
    vm_link --arch="x86_64" \
            --boot="uefi" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="macos" \
            --disktype="qcow2" \
            --headless="no" \
            --os-type="generic" \
            --os-variant="generic" \
            --ram="2048" \
            --title="macOS" \
            --vcpus=4

    ############################################################################
    # Servers
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="Debian-9-Server" \
            --disktype="qcow2" \
            --headless="yes" \
            --os-type="linux" \
            --os-variant="debian10" \
            --ram="12288" \
            --static-ip="192.168.122.2" \
            --static-mac="52:54:00:37:5e:1e" \
            --title="Debian_9_Server" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="freedombox-bullseye-free_all-amd64" \
            --disktype="qcow2" \
            --headless="yes" \
            --os-type="linux" \
            --os-variant="debian10" \
            --ram="8192" \
            --static-ip="192.168.122.5" \
            --static-mac="52:54:00:37:ff:ff" \
            --title="FreedomBox_Server" \
            --vcpus="$(nproc)"
}