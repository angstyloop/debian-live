#!/bin/bash
# Depends on load_hw_cfg                  (cfg/cfg.sh)
# Depends on read_net_cfg_from_csv        (util/net.sh)
# Depends on append_unique_string_to_file (util/fs.sh)
# Ensure domain resolution
function add_hosts {
    append_unique_string_to_file "$IP_ROUTER_ETH router router.local" /etc/hosts sudo
    append_unique_string_to_file "$IP_DEBIAN_SERVER_VM debian-server $DOMAIN-internal" /etc/hosts sudo
    append_unique_string_to_file "$IP_NAS_ETH readynas" /etc/hosts sudo
    append_unique_string_to_file "$IP_FREEDOMBOX_SERVER_VM freedombox-server" /etc/hosts sudo
    if [ "$PRESET" != "server" ]; then
        append_unique_string_to_file "$IP_PHONE_WIFI phone P2" /etc/hosts sudo
        append_unique_string_to_file "$IP_REFORM_ETH reform" /etc/hosts sudo
        append_unique_string_to_file "$IP_LIBREM13 l13" /etc/hosts sudo
        append_unique_string_to_file "$IP_ROKU_STICK_WIFI roku-stick" /etc/hosts sudo
        append_unique_string_to_file "$IP_R220_1_ETH r220-eno1" /etc/hosts sudo
        append_unique_string_to_file "$IP_R220_2_ETH r220-eno2" /etc/hosts sudo
        append_unique_string_to_file "$IP_DELL_PRINTER_ETH dell-1815dn" /etc/hosts sudo
        append_unique_string_to_file "$IP_ASUS_U52F_WIFI asus-u52f-wls1" /etc/hosts sudo
        append_unique_string_to_file "$IP_ASUS_U52F_ETH asus-u52f-ens5" /etc/hosts sudo
        append_unique_string_to_file "$IP_DONGLE_100M_ETH usb-300m" /etc/hosts sudo
        append_unique_string_to_file "$IP_LIBREM_MINI_ETH lm-eth" /etc/hosts sudo
        append_unique_string_to_file "$IP_LIBREM_MINI_WIFI lm-wifi" /etc/hosts sudo
        append_unique_string_to_file "$IP_ROKU_ULTRA_ETH roku-ultra-eth" /etc/hosts sudo
        append_unique_string_to_file "$IP_ROKU_ULTRA_WIFI roku-ultra-wifi" /etc/hosts sudo
        append_unique_string_to_file "$IP_STEAM_LINK_ETH steam-link-eth" /etc/hosts sudo
        append_unique_string_to_file "$IP_STEAM_LINK_WIFI steam-link-wifi" /etc/hosts sudo
        append_unique_string_to_file "$IP_R220_IDRAC_ETH r220-idrac" /etc/hosts sudo
        return
    fi
}