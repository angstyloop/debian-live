# debian-live

### Overview

The purpose of this project is to run an official/vanilla Debian live install environment and configure it such that it is suitable for everyday use.
These tools customize an immutable Operating System (OS) with optional filesystem persistence and numerous quality-of-life improvements.
Boot the computer, run the script, and you're done.

Use this repository as a starting point and customize it to suit your needs.

### Features

The following is a general summary and not an exhaustive list of features:

- Configuration presets for desktop/laptop and server environments
- Mount devices in configured locations, re-mounting when necessary
- Virtual filesystem support (qcow2)
- Configure persistent storage if configured and available
- Configure upstream software repositories; will also use a local apt-mirror if available
- Install and remove software based on configurable package lists
- Organize GNOME application icons into folders
- Configure existing VMs for use with `virt-manager` (or headless if using a server as a host device)
- Create and mount an optional swapfile

### Usage

1. Write a Debian Live image to your boot drive (should be approx 1-4 GiB)
    - Note: See tested environments below
    - Disclaimer: use caution and seek assistance if necessary to do this; the authors take no responsibility for lost data
2. Optional: Write one or more ext4 partitions to fill the remaining space on the boot drive
3. Review the high-level configuration file located at `./cfg/cfg.sh` and edit as necessary
4. Execute the main script located at `./setup-debian.sh`

### System requirements

- Host x86_64 computer (to run natively or within a Virtual Machine)
- 8GB+ RAM recommended
- Debian Live system image
    - may be booted from external media (normal use) or natively on the host computer (by copying the rootfs partition to disk and booting to it)
    - Debian 10 and 11, GNOME desktop
    - Debian 10 and 11, standard (no graphical environment)

### TODO

1. Resolve many TODO statements sprinkled throughout the codebase.
2. Get data import/configuration running smoothly on live instance:
    - pidgin
    - evolution
    - transmission
